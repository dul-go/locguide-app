package main

import (
	"fmt"
	"text/template"

	"gitlab.oit.duke.edu/dul-go/httpresponse"
	"gitlab.oit.duke.edu/dul-go/journal"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
	"gitlab.oit.duke.edu/dul-go/locguide-app/model"
)

var _ Controller = &AreaMapController{}

type AreaMapController struct {
}

func (a *AreaMapController) PathPrefix() string {
	return "/areamaps/"
}

func (a *AreaMapController) Listing(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	var areaMaps []model.AreaMap
	err := svc.FindThese(&areaMaps)

	if err != nil {
		return nil, fmt.Errorf("(AreaMap) Listing: unable to find area maps")
	}

	pageDocument := &httpresponse.PageDocument{Title: "Area Maps", Body: []byte("")}
	bodyTemplate := template.Must(template.ParseFiles("tmpl/areamaps.go.html"))

	c.AddTemplateData("AreaMaps", areaMaps)
	c.AddTemplateData("PlaceholderImage", "https://via.placeholder.com/150")

	err = bodyTemplate.Execute(pageDocument, c.TemplateData())
	if err != nil {
		return nil, fmt.Errorf("(AreaMap) Listing: unable to execute template - %v", err)
	}

	return pageDocument, nil

}

func (a *AreaMapController) Form(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	var areaMap model.AreaMap
	err := svc.FirstOrInit(&areaMap, c.IdForRequestedResource())
	if err != nil {
		return nil, fmt.Errorf("(AreaMap) Form: uanble to load area map (%v)", err)
	}

	if c.IsPostRequest() {
		// save data from the form

		areaMap.ScanForm(c.Params(), "")
		err := svc.Save(&areaMap)
		if err != nil {
			return nil, fmt.Errorf("AreaMapForm: unable to save Area Map - %v", err)
		}

		c.SetSessionKey("success_message", "Changes have been saved.")
		c.Redirect(fmt.Sprintf("%s%d", a.PathPrefix(), areaMap.ID), 303)
	}

	buildings, err := svc.ActiveBuildings()
	for _, v := range buildings {
		journal.Debug.Printf("%v\n", v)
	}

	c.AddTemplateData("AreaMap", areaMap)
	c.AddTemplateData("Entity", areaMap)
	c.AddTemplateData("Buildings", buildings)

	formButtons := NewFormButtons("tmpl/form_buttons.go.html", c)
	if err = formButtons.Render(c.TemplateData()); err != nil {
		panic(err)
	}
	c.AddTemplateData("FormButtons", formButtons.Buttons)

	pageTitle := "Area Map / " + areaMap.Building + " / " + areaMap.Area

	pageDocument := &httpresponse.PageDocument{Title: pageTitle, Body: []byte("")}
	bodyTemplate := template.Must(template.ParseFiles("tmpl/areamap.go.html"))
	err = bodyTemplate.Execute(pageDocument, c.TemplateData())
	if err != nil {
		return nil, fmt.Errorf("(AreaMap) Form: unable to execute template - %v", err)
	}

	return pageDocument, nil
}

func (a *AreaMapController) DeleteHandler(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	err := svc.Delete(&model.AreaMap{}, c.IdForRequestedResource())
	if err != nil {
		return nil, fmt.Errorf("AreaMap Delete: unable to delete area map - %v\n", err)
	}

	c.SetSessionKey("success_message", "The selected area map has been deleted.")
	c.Redirect(fmt.Sprintf("%s", a.PathPrefix()), 303)

	pageDocument := &httpresponse.PageDocument{Title: "Delete Area Map", Body: []byte("")}
	return pageDocument, nil
}
