package main

import (
	"context"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"text/template"
	"time"

	"github.com/crewjam/saml/samlsp"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"gitlab.oit.duke.edu/dul-go/httpresponse"
	"gitlab.oit.duke.edu/dul-go/journal"
	"gitlab.oit.duke.edu/dul-go/locguide-app/config"
)

type App struct {
	// DB is our database service, designed
	// to separate driver-specific implementation
	// from the route controllers
	DBSvc *DatabaseService

	// GracefulWait is the timeout value applied when
	// the app is SIGINT'ed to terminate
	// Used in main() when flags are parsed
	GracefulWait time.Duration

	// AssetsDir is where our public assets live
	// (css, images, js)
	// Used in main() when flags are parsed
	AssetsDir string

	// MainTemplateName is the filename of the app's
	// main layout.
	// Used in main() when flags are parsed
	MainTemplateName string

	// Debug is self-explanatory
	// Used in main() when flags are parsed
	Debug bool

	// Router stores all of our menu routing options
	// see App.initializeRoutes()
	router *mux.Router

	// config is the application's config.
	config config.Config

	// mainTemplate will store the MAIN DUL LAYOUT template here
	// (so as to be efficient and not call this for every handler)
	// and have the template's "Execute" write into 'w'
	// prior to deferring to the app-specific handler
	mainTemplate *template.Template

	// SAML middleware is intiailized in
	// App.initializeSaml
	samlSP *samlsp.Middleware

	// App-wide session
	session *sessions.Session
}

func (a *App) Initialize(c config.Config) *App {
	// verify (or wait) for database port to be available
	// or return an error
	dbport := []string{
		fmt.Sprintf("%s:%s", c.Database.Host, c.Database.Port),
	}
	journal.Info.Printf("Waiting %d secs for database, %s, to be available...\n", 30, dbport[0])
	err := WaitForServices(dbport, 30*time.Second)
	if err != nil {
		journal.Error.Printf("Unable to establish db connection at %s:%s\n", c.Database.Host, c.Database.Port)
		os.Exit(1)
	} else {
		journal.Info.Println("Database available. Let's proceed!")
	}
	svc, err := NewDatabaseService(c)
	if err != nil {
		panic("Unable to establish database connection")
	}
	a.DBSvc = svc

	a.config = c
	a.router = mux.NewRouter()

	a.session = sessions.NewSession(
		sessions.NewCookieStore([]byte(a.config.SessionKey)),
		"LG_SESSION",
	)

	a.initializeMainTemplate()
	a.initializeSaml()
	a.initializeRoutes()

	// return the app for function-chaining
	return a
}

// Run starts an http.Server, calls ListenAndServe
func (a *App) Run() {

	// create the server
	addr := fmt.Sprintf("%s:%s", a.config.GetServerBindHost(), a.config.GetServerBindPort())

	// see tls.go
	tlsCfg := TlsConfig()

	srv := &http.Server{
		Handler: handlers.LoggingHandler(os.Stdout, a.router),
		Addr:    addr,
		// Good practice: enforce timeouts for servers
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		TLSConfig:    tlsCfg,
	}
	journal.Info.Printf("listening to %s. Happy serving.\n", addr)

	// Run our server in a goroutine so that it doesn't block
	go func() {
		var err error
		if a.config.Server.ForceSSL || a.config.Server.Port == "443" {
			// ssl (https) preferred
			err = srv.ListenAndServeTLS(a.config.Server.SSLCertFile, a.config.Server.SSLKeyFile)
		} else {
			// run in http mode
			err = srv.ListenAndServe()
		}
		if err != nil {
			journal.Error.Println(err)
		}
		//log.Fatal(srv.ListenAndServe())
	}()

	c := make(chan os.Signal, 1)

	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT, or SIGTERM (Ctrl+/) will not be caught
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal
	<-c

	// create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), a.GracefulWait)
	defer cancel()
	srv.Shutdown(ctx)
	journal.Info.Println("shutting down gracefully")
}

func (a *App) HasDatabaseConnection() bool {
	return a.DBSvc != nil
}

func (a *App) initializeSaml() {
	journal.Info.Println("intializing SAML setup...")
	journal.Info.Printf("(SAML) reading cert file [%s] and key file [%s]...\n", a.config.SAML.CertFile, a.config.SAML.KeyFile)

	keyPair, err := tls.LoadX509KeyPair(a.config.SAML.CertFile, a.config.SAML.KeyFile)
	if err != nil {
		panic(err)
	}
	keyPair.Leaf, err = x509.ParseCertificate(keyPair.Certificate[0])
	if err != nil {
		panic(err)
	}
	idpMetadataURL, err := url.Parse(a.config.SAML.IdpUrl)
	if err != nil {
		panic(err)
	}
	idpMetadata, err := samlsp.FetchMetadata(context.Background(), http.DefaultClient, *idpMetadataURL)
	if err != nil {
		panic(err)
	}
	rootURL, err := url.Parse(fmt.Sprintf("https://%s", a.config.Server.Name))
	if err != nil {
		panic(err)
	}
	a.samlSP, err = samlsp.New(samlsp.Options{
		URL:         *rootURL,
		EntityID:    a.config.SAML.EntityID,
		Key:         keyPair.PrivateKey.(*rsa.PrivateKey),
		Certificate: keyPair.Leaf,
		IDPMetadata: idpMetadata,
	})
	if err != nil {
		journal.Error.Printf("initializing SAML failed: %v\n", err)
		panic(err)
	}
	journal.Info.Printf("SAML initialized for https://%s\n", a.config.Server.Name)
}

func (a *App) initializeRoutes() {
	// WEB ROUTING begins here
	// ----------------------------------------------------------
	a.router.PathPrefix("/assets/").
		Handler(http.StripPrefix("/assets/", http.FileServer(http.Dir(a.AssetsDir))))

	if a.samlSP != nil {
		// equivalent of defining <Location /Shibboleth.sso>...</Location> in
		// our typical Apache configurations
		a.router.PathPrefix("/saml/").Handler(a.samlSP)

		// Throwaway test handler to verify saml was working
		helloApp := http.HandlerFunc(hello)
		a.router.PathPrefix("/hello").Handler(a.samlSP.RequireAccount(helloApp))
	}

	// Add as many handlers as needed
	////a.router.Handle("/", a.samlSP.RequireAccount(a.pageOrError(ShowWelcomePage)))
	a.router.Handle("/", a.samlSP.RequireAccount(a.Rendered(WelcomePage)))

	// API and/or JSON handlers
	a.router.HandleFunc("/locguide/mapinfo", mapinfo)
	a.router.HandleFunc("/mapinfo", mapinfo)

	st := a.router.PathPrefix("/typeahead").Subrouter()
	st.HandleFunc("/search/{q}", a.TypeaheadHandler("searchbar")).Methods("GET", "PUT", http.MethodPatch, http.MethodOptions)
	st.HandleFunc("/callno/{q}", a.TypeaheadHandler("callno")).Methods("GET", "PUT", http.MethodPatch, http.MethodOptions)
	st.Use(mux.CORSMethodMiddleware(st))

	s := mux.NewRouter().PathPrefix("/").Subrouter().StrictSlash(true)

	controllers := []Controller{
		&CollectionController{},
		&CollectionGroupController{},
		&GenericMessageController{},
		&ExternalLinkController{},
		&AreaMapController{},
		&LocationMapController{},
	}

	for _, c := range controllers {
		prefix := c.PathPrefix()
		s.HandleFunc(prefix, a.Rendered(c.Listing))
		s.HandleFunc(prefix+"new", a.Rendered(c.Form)).Methods("GET", "POST")
		s.HandleFunc(prefix+"{id:[0-9]+}", a.Rendered(c.Form)).Methods("GET", "POST")
		s.HandleFunc(prefix+"delete/{id:[0-9]+}", a.Rendered(c.DeleteHandler))
	}
	a.router.PathPrefix("/").Handler(a.samlSP.RequireAccount(s))

	// The above loop is roughly equivalent to this (below):
	/*
		cr := MyResourceController{}
		s.HandleFunc("/collections/", a.Rendered(cr.Listing))
		s.HandleFunc("/collections/new", a.Rendered(cr.Form)).Methods("GET", "POST")
		s.HandleFunc("/collections/{id:[0-9]+}", a.Rendered(cr.Form)).Methods("GET", "POST")
		s.HandleFunc("/collections/delete/{id:[0-9]+}", a.Rendered(cr.DeleteHandler))
	*/
	// a.router.Use(handlers.LoggingHandler(os.Stdout, a.router))
}

func (a *App) initializeMainTemplate() {
	a.mainTemplate = template.Must(template.ParseFiles(a.MainTemplateName))
}

// Rendered processes the PageHandler, h, using its output to render
// our main template
// Creates a main template then executes the function handler (f)
// to receive a data context (interface{}) which is then applied to the
// main template
// Uses a context.Context to share config data (database credentials, etc)
// returns a 'handler' function that processes the request
// SOURCE TIP: https://golang.org/doc/articles/wiki/#tmp_9
func (a *App) Rendered(h PageHandler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := a.NewContext(w, r)
		pageDocument, err := h(c)
		tmplData := c.TemplateData()

		// Get the displayName attribute from the samlsp session
		if a.samlSP != nil {
			tmplData["AuthDisplayName"] = samlsp.AttributeFromContext(r.Context(), "displayName")
			tmplData["AuthGivenName"] = samlsp.AttributeFromContext(r.Context(), "givenName")
		}

		if err != nil {
			errBox := NewErrorMessageBox("tmpl/errormessage.go.html")
			errBox.Render(map[string]interface{}{
				"message": err.Error(),
			})

			tmplData["Page"] = httpresponse.PageDocument{Title: "Internal Server Error", Body: []byte(errBox.Content)}
			w.WriteHeader(http.StatusInternalServerError)
			journal.Error.Printf("%s\n", err.Error())
			//http.Error(w, err.Error(), http.StatusInternalServerError)
			//return
		} else {
			// finally, we'll execute the 'mainTemplate' with the context
			// returned by 'f', which should have a "Body" accessor.
			tmplData["Page"] = pageDocument
		}

		//for k, v := range tmplData {
		//	journal.Debug.Printf("%s => [%v]\n", k, v)
		//}
		err = a.mainTemplate.Execute(w, tmplData)

		c.SaveSession()

		if err != nil {
			journal.Error.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

func mapinfo(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]bool{"ok": true})
}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %s!", samlsp.AttributeFromContext(r.Context(), "displayName"))
}
