FROM golang:1.17-buster

# switch to the root user 
# to setup the image
USER 0

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

RUN apt-get update && apt-get install -y wait-for-it \
    && rm -rf /var/lib/apt/lists/*

# Move to working directory /build
WORKDIR /build

# Copy and download dependencies using go mod
COPY go.mod .
COPY go.sum .
RUN go mod download

# Copy the code into the container
COPY . .

# Build the application
RUN go build -o main .

# Move to /dist as the place for resulting binary folder
WORKDIR /dist
RUN mkdir sp
COPY sp.crt /dist/sp/
COPY sp.key /dist/sp/
RUN chmod a+r /dist/sp/sp.key

RUN mkdir ssl
COPY server.pem /dist/ssl/
COPY server.key /dist/ssl/
RUN chmod a+r /dist/ssl/server.key

# Copy binary from build to main folder
RUN cp /build/main .
RUN cp /build/config.yml .
RUN cp -r /build/assets .
RUN cp -r /build/tmpl .
#RUN cp /build/server.pem .
#RUN cp /build/server.key .

COPY docker-entrypoint.sh /usr/bin/
RUN chmod a+x /usr/bin/docker-entrypoint.sh

# Export necessary port
EXPOSE 8443
EXPOSE 8080

# make sure to switch back to a non-privileged user
# to run the container (for security's sake)
USER 1001

# ENTRYPOINT ["/usr/bin/docker-entrypoint.sh"]

# Command to run when starting the container
CMD ["/dist/main"]

