package main

import (
	"fmt"
	"strconv"
	"text/template"

	"gitlab.oit.duke.edu/dul-go/httpresponse"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
	"gitlab.oit.duke.edu/dul-go/locguide-app/model"
)

var _ Controller = &CollectionGroupController{}

type CollectionGroupController struct{}

func (c *CollectionGroupController) PathPrefix() string {
	return "/collectiongroups/"
}

func (cg *CollectionGroupController) Listing(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	var collectionGroups []model.CollectionGroup
	pager := NewPagination(model.CollectionGroup{}, app.config)
	err := svc.Paginate(pager, c.Params()).FindThese(&collectionGroups)

	if err != nil {
		return nil, fmt.Errorf("CollectionGroup Listing: unable to find collection groups")
	}

	pager.Render(map[string]interface{}{
		"WorkingURL": c.Request().URL.Path,
		"Pager":      pager,
	})
	c.AddTemplateData("Pager", pager.Content)

	pageDocument := &httpresponse.PageDocument{Title: "Collection Group", Body: []byte("")}
	bodyTemplate := template.Must(template.ParseFiles("tmpl/collectiongroups.go.html"))
	err = bodyTemplate.Execute(pageDocument, collectionGroups)
	if err != nil {
		return nil, fmt.Errorf("Listing (CollectionGroup): unable to execute template - %v", err)
	}

	return pageDocument, nil
}

func (cg *CollectionGroupController) Form(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	var collectionGroup model.CollectionGroup
	err := svc.FirstOrInit(&collectionGroup, c.IdForRequestedResource())
	if err != nil {
		return nil, fmt.Errorf("CollectionGroup Form: unable to load collection group record - %v", err)
	}

	if c.IsPostRequest() {
		collectionGroup.ScanForm(c.Params(), "")
		err := svc.Save(&collectionGroup)
		if err != nil {
			return nil, fmt.Errorf("Form (Colllection Group): unable to save collection group - %v", err)
		}

		c.SetSessionKey("success_message", "Change have been saved")
		c.Redirect(fmt.Sprintf("%s%d", cg.PathPrefix(), collectionGroup.ID), 303)
	}

	c.AddTemplateData("CollectionGroup", collectionGroup)
	c.AddTemplateData("Entity", collectionGroup)

	formButtons := NewFormButtons("tmpl/form_buttons.go.html", c)
	if err = formButtons.Render(c.TemplateData()); err != nil {
		panic(err)
	}
	c.AddTemplateData("FormButtons", formButtons.Buttons)

	pageTitle := "Collection Group / " + strconv.Itoa(int(collectionGroup.ID))
	pageDocument := &httpresponse.PageDocument{Title: pageTitle, Body: []byte("")}
	bodyTemplate := template.Must(template.ParseFiles("tmpl/collectiongroup.go.html"))
	err = bodyTemplate.Execute(pageDocument, c.TemplateData())
	if err != nil {
		return nil, fmt.Errorf("Form (Collection): unable to execute template:<br />\n %v", err)
	}

	return pageDocument, nil
}

func (cg *CollectionGroupController) DeleteHandler(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	err := svc.Delete(&model.CollectionGroup{}, c.IdForRequestedResource())
	if err != nil {
		return nil, fmt.Errorf("CollectionGroup Delete: unable to delete external link - %v\n", err)
	}

	c.SetSessionKey("success_message", "The selected collection group has been deleted.")
	c.Redirect(fmt.Sprintf("%s", cg.PathPrefix()), 303)

	pageDocument := &httpresponse.PageDocument{Title: "Delete External Link", Body: []byte("")}
	return pageDocument, nil
}
