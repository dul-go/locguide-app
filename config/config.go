package config

import (
	"fmt"
	"os"
	"strings"

	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v2"
)

type Config struct {
	AppTitle    string `yaml:"app_title"`
	WaitTimeout int    `yaml:"wait_timeout" envconfig:"WAIT_TIMEOUT"`
	SetupDB     bool   `yaml:"setup_db" default:"false" envconfig:"SETUP_DB"`
	Database    struct {
		Driver     string   `yaml:"driver"`
		Host       string   `yaml:"host" envconfig:"LOCGUIDE_DBHOST"`
		Port       string   `yaml:"port" default:"3306" envconfig:"LOCGUIDE_DBPORT"`
		Username   string   `yaml:"user" envconfig:"LOCGUIDE_DBUSER"`
		Password   string   `yaml:"pass" envconfig:"LOCGUIDE_DBPASS"`
		DbName     string   `yaml:"dbname" envconfig:"LOCGUIDE_DBNAME"`
		DriverOpts []string `yaml:"driver_opts"`
		Pager      struct {
			PagesPerChapter int    `yaml:"pages_per_chapter"`
			PageQueryName   string `yaml:"page_query_name"`
		} `yaml:"pager"`
	} `yaml:"database"`
	Server struct {
		Host        string `yaml:"bind_host" default:"0.0.0.0" envconfig:"LOCGUIDE_BIND_HOST"`
		Port        string `yaml:"bind_port" envconfig:"LOCGUIDE_BIND_PORT"`
		Name        string `yaml:"name"`
		ForceSSL    bool   `yaml:"force_ssl" envconfig:"LOCGUIDE_FORCE_SSL"`
		SSLCertFile string `yaml:"ssl_cert_file" default:"ssl/server.pem" envconfig:"SSL_CERT_FILE"`
		SSLKeyFile  string `yaml:"ssl_key_file" default:"ssl/server.key" envconfig:"SSL_KEY_FILE"`
	} `yaml:"server"`
	SAML struct {
		IdpUrl   string `yaml:"idp_url"`
		CertFile string `yaml:"cert_file" default:"sp/sp.crt" envconfig:"SAML_CERT_FILE"`
		KeyFile  string `yaml:"key_file" default:"sp/sp.key" envconfig:"SAML_KEY_FILE"`
		EntityID string `yaml:"entity_id"`
	} `yaml:"saml"`
	UI struct {
		CSS struct {
			ContainerClass string `yaml:"container_class"`
		} `yaml:"css"`
	} `yaml:"ui"`
	SessionKey string `yaml:"session_key" envconfig:"LOCGUIDE_SESS_KEY"`
}

func (cfg Config) String() string {
	return fmt.Sprintf("{%s {%s %s %s %s [password redacted] %s %v} %v} page_query_name=[%s] setup_db=[%t]",
		cfg.AppTitle, cfg.Database.Driver, cfg.Database.Host,
		cfg.Database.Port, cfg.Database.Username, cfg.Database.DbName, cfg.Database.DriverOpts,
		cfg.Server, cfg.Database.Pager.PageQueryName, cfg.SetupDB)
	// {Location Map Finder {mysql localhost 3306 golang password django_dev [parseTime=true charset=utf8]} {0.0.0.0 5000}}
}

// GetDatabaseDSN returns database dsn string
func (cfg Config) GetDatabaseDSN() string {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s",
		cfg.Database.Username,
		cfg.Database.Password,
		cfg.Database.Host,
		cfg.Database.Port,
		cfg.Database.DbName,
	)
	if len(cfg.Database.DriverOpts) > 0 {
		dsn = dsn + "?" + strings.Join(cfg.Database.DriverOpts, "&")
	}
	return dsn
}

// GetDriver returns database driver string (i.e. mysql)
func (cfg Config) GetDriver() string {
	return cfg.Database.Driver
}

// GetDatabaseHost returns DB hostname
func (cfg Config) GetDatabaseHost() string {
	return cfg.Database.Host
}

// GetTitle returns title attribute (app_title)
func (cfg Config) GetTitle() string {
	return cfg.GetAppTitle()
}

func (cfg Config) GetAppTitle() string {
	return cfg.AppTitle
}

// GetServerBindPort returns server port (8000, etc)
func (cfg Config) GetServerBindPort() string {
	return cfg.Server.Port
}

// GetServerBindHost returns binding host address
func (cfg Config) GetServerBindHost() string {
	return cfg.Server.Host
}

func ReadConfigFile(cfg *Config, filename string) {
	f, err := os.Open(filename)
	if err != nil {
		processError(err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)
	if err != nil {
		processError(err)
	}
}

func ReadEnv(cfg *Config) {
	err := envconfig.Process("", cfg)
	if err != nil {
		processError(err)
	}
}

// processError prints error and exits the program
func processError(err error) {
	fmt.Println(err)
	os.Exit(2)
}
