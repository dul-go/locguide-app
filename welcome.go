package main

import (
	"fmt"
	"text/template"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.oit.duke.edu/dul-go/httpresponse"
	"gitlab.oit.duke.edu/dul-go/locguide-app/config"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
)

//WelcomePage returns a PageDocument with a basic title
func WelcomePage(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc
	conf := c.Value("config").(config.Config)

	buildings, err := svc.ActiveBuildings()
	buildings2, err := svc.ActiveBuildings2()
	if err != nil {
		panic(err)
	}

	pageDocument := &httpresponse.PageDocument{Title: conf.AppTitle, Body: []byte("")}
	bodyTemplate := template.Must(template.ParseFiles("tmpl/welcome.html"))

	c.AddTemplateData("Buildings", buildings)
	c.AddTemplateData("Buildings2", buildings2)

	err = bodyTemplate.Execute(pageDocument, c.TemplateData())

	// If an error is encountered, simply return the error...
	if err != nil {
		return nil, fmt.Errorf("ShowWelcomePage: unable to execute template for Welcome Page - %v", err)
	}

	// Otherwise, return the pageDocument structure (title, body, etc)
	return pageDocument, nil
}
