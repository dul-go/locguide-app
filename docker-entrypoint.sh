#!/bin/sh

set -e

wait-for-it db:3306 --
exec "$@"
