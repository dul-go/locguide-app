-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: django
-- ------------------------------------------------------
-- Server version	5.1.61

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_log`
--

DROP TABLE IF EXISTS `admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_log_user_id` (`user_id`),
  KEY `admin_log_content_type_id` (`content_type_id`),
  CONSTRAINT `admin_log_ibfk_1` FOREIGN KEY (`content_type_id`) REFERENCES `content_type` (`id`),
  CONSTRAINT `admin_log_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=182946 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appdocs_application`
--

DROP TABLE IF EXISTS `appdocs_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appdocs_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `status` varchar(60) NOT NULL,
  `language` varchar(40) NOT NULL,
  `framework` varchar(40) NOT NULL,
  `db_type` varchar(40) NOT NULL,
  `db_name` varchar(100) NOT NULL,
  `db_user` varchar(40) NOT NULL,
  `vcs_url` varchar(200) NOT NULL,
  `func_owner` varchar(200) NOT NULL,
  `developer` varchar(255) NOT NULL,
  `admin_url` varchar(200) NOT NULL,
  `public_url` varchar(200) NOT NULL,
  `doc_url` varchar(200) NOT NULL,
  `install_path` varchar(255) NOT NULL,
  `log` varchar(255) NOT NULL,
  `notes` longtext NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=392 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `permission_id_refs_id_5886d21f` (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2682 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_message`
--

DROP TABLE IF EXISTS `auth_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_message_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100682 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_content_type_id` (`content_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2642 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=16084 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `group_id_refs_id_f116770` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1384 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `permission_id_refs_id_67e79cb` (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=634 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `authority_lists_authoritylist`
--

DROP TABLE IF EXISTS `authority_lists_authoritylist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authority_lists_authoritylist` (
  `title` varchar(50) NOT NULL,
  `group_id` varchar(50) DEFAULT NULL,
  `description` longtext NOT NULL,
  `use_values_for_display` tinyint(1) NOT NULL,
  `updateable` tinyint(1) NOT NULL,
  PRIMARY KEY (`title`),
  UNIQUE KEY `title` (`title`,`group_id`),
  KEY `authority_lists_authoritylist_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `authority_lists_authoritylistgroup`
--

DROP TABLE IF EXISTS `authority_lists_authoritylistgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authority_lists_authoritylistgroup` (
  `title` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `authority_lists_authorityterm`
--

DROP TABLE IF EXISTS `authority_lists_authorityterm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authority_lists_authorityterm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL,
  `display` varchar(255) NOT NULL,
  `authority_list_id` varchar(50) NOT NULL,
  `sort_order` int(10) unsigned DEFAULT NULL,
  `blacklisted` tinyint(1) NOT NULL,
  `recommended` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `authority_list_id` (`authority_list_id`,`value`),
  KEY `authority_lists_authorityterm_authority_list_id` (`authority_list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=78332 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `catalog_excluded_barcodes`
--

DROP TABLE IF EXISTS `catalog_excluded_barcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_excluded_barcodes` (
  `ebid` int(11) NOT NULL AUTO_INCREMENT,
  `barcode` varchar(20) NOT NULL,
  `ignore` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ebid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `colldev_requests_emailrecord`
--

DROP TABLE IF EXISTS `colldev_requests_emailrecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colldev_requests_emailrecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_request_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  `recorded` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `colldev_requests_emailrecord_8290bd9` (`title_request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24932 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `colldev_requests_format`
--

DROP TABLE IF EXISTS `colldev_requests_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colldev_requests_format` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `value` (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `colldev_requests_librarian`
--

DROP TABLE IF EXISTS `colldev_requests_librarian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colldev_requests_librarian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `initials` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `person_id` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=332 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `colldev_requests_status`
--

DROP TABLE IF EXISTS `colldev_requests_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colldev_requests_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `value` (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `colldev_requests_subject`
--

DROP TABLE IF EXISTS `colldev_requests_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colldev_requests_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `value` (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=1180 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `colldev_requests_titlerequest`
--

DROP TABLE IF EXISTS `colldev_requests_titlerequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colldev_requests_titlerequest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requester_name` varchar(255) NOT NULL,
  `requester_netid` varchar(20) NOT NULL,
  `requester_uniqueid` varchar(20) NOT NULL,
  `requester_affiliation` varchar(20) NOT NULL,
  `requester_dept` varchar(255) NOT NULL,
  `requester_address` longtext NOT NULL,
  `requester_phone` varchar(80) NOT NULL,
  `requester_email` varchar(75) NOT NULL,
  `title` varchar(200) NOT NULL,
  `author` varchar(200) NOT NULL,
  `edition` varchar(100) NOT NULL,
  `format_id` int(11) NOT NULL,
  `other_format` varchar(100) NOT NULL,
  `volumes` varchar(100) NOT NULL,
  `isbn` varchar(13) NOT NULL,
  `issn` varchar(8) NOT NULL,
  `publisher` varchar(200) NOT NULL,
  `pub_place` varchar(100) NOT NULL,
  `pub_year` varchar(50) NOT NULL,
  `info_source` varchar(255) NOT NULL,
  `additional_info` longtext NOT NULL,
  `notes` longtext NOT NULL,
  `librarian_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `colldev_requests_titlerequest_b3d51e61` (`format_id`),
  KEY `colldev_requests_titlerequest_73602d6f` (`librarian_id`),
  KEY `colldev_requests_titlerequest_44224078` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12546 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `colldev_requests_titlerequest_subject`
--

DROP TABLE IF EXISTS `colldev_requests_titlerequest_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colldev_requests_titlerequest_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titlerequest_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `titlerequest_id` (`titlerequest_id`,`subject_id`),
  KEY `colldev_requests_titlerequest_subject_7df48e5f` (`titlerequest_id`),
  KEY `colldev_requests_titlerequest_subject_638462f1` (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41974 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comment_flags`
--

DROP TABLE IF EXISTS `comment_flags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_flags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `flag_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`comment_id`,`flag`),
  KEY `comment_flags_user_id` (`user_id`),
  KEY `comment_flags_comment_id` (`comment_id`),
  KEY `comment_flags_flag` (`flag`),
  CONSTRAINT `comment_flags_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_type_id` int(11) NOT NULL,
  `object_pk` longtext NOT NULL,
  `site_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_email` varchar(75) NOT NULL,
  `user_url` varchar(200) NOT NULL,
  `comment` longtext NOT NULL,
  `submit_date` datetime NOT NULL,
  `ip_address` char(15) DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL,
  `is_removed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_content_type_id` (`content_type_id`),
  KEY `comments_site_id` (`site_id`),
  KEY `comments_user_id` (`user_id`),
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`content_type_id`) REFERENCES `content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1856 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conservation_item`
--

DROP TABLE IF EXISTS `conservation_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conservation_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `surveyor` varchar(255) NOT NULL,
  `surveyed_on` date NOT NULL,
  `sent_to_conservation` tinyint(1) NOT NULL,
  `date_sent_to_conservation` date DEFAULT NULL,
  `barcode` varchar(20) NOT NULL,
  `call_number` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `publication_year` varchar(40) NOT NULL,
  `size` decimal(4,1) NOT NULL,
  `thickness` decimal(3,1) NOT NULL,
  `enclosure_id` int(11) NOT NULL,
  `enclosure_other` varchar(255) NOT NULL,
  `enclosure_condition_id` int(11) NOT NULL,
  `enclosure_recommendation_id` int(11) NOT NULL,
  `primary_covering_material_id` int(11) NOT NULL,
  `covers_other` varchar(255) NOT NULL,
  `binding_style_id` int(11) NOT NULL,
  `binding_style_other` varchar(255) NOT NULL,
  `text_to_cover_attachment_id` int(11) NOT NULL,
  `ttca_other` varchar(255) NOT NULL,
  `textblock_material_other` varchar(255) NOT NULL,
  `media_other` varchar(255) NOT NULL,
  `additions_other` varchar(255) NOT NULL,
  `binding_condition_other` varchar(255) NOT NULL,
  `textblock_condition_other` varchar(255) NOT NULL,
  `conditions_of_use_id` int(11) NOT NULL,
  `skill_level_required_id` int(11) NOT NULL,
  `conservation_priority_id` int(11) NOT NULL,
  `comments` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `conservation_item_survey_id` (`survey_id`),
  KEY `conservation_item_enclosure_id` (`enclosure_id`),
  KEY `conservation_item_enclosure_condition_id` (`enclosure_condition_id`),
  KEY `conservation_item_enclosure_recommendation_id` (`enclosure_recommendation_id`),
  KEY `conservation_item_primary_covering_material_id` (`primary_covering_material_id`),
  KEY `conservation_item_binding_style_id` (`binding_style_id`),
  KEY `conservation_item_text_to_cover_attachment_id` (`text_to_cover_attachment_id`),
  KEY `conservation_item_conditions_of_use_id` (`conditions_of_use_id`),
  KEY `conservation_item_skill_level_required_id` (`skill_level_required_id`),
  KEY `conservation_item_conservation_priority_id` (`conservation_priority_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5212 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conservation_item_additions`
--

DROP TABLE IF EXISTS `conservation_item_additions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conservation_item_additions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `optionvalue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_id` (`item_id`,`optionvalue_id`),
  KEY `optionvalue_id_refs_id_5791824d` (`optionvalue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11774 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conservation_item_binding_condition`
--

DROP TABLE IF EXISTS `conservation_item_binding_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conservation_item_binding_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `optionvalue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_id` (`item_id`,`optionvalue_id`),
  KEY `optionvalue_id_refs_id_9c8bfbb` (`optionvalue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22214 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conservation_item_covers`
--

DROP TABLE IF EXISTS `conservation_item_covers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conservation_item_covers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `optionvalue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_id` (`item_id`,`optionvalue_id`),
  KEY `optionvalue_id_refs_id_7069eedd` (`optionvalue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16424 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conservation_item_media`
--

DROP TABLE IF EXISTS `conservation_item_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conservation_item_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `optionvalue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_id` (`item_id`,`optionvalue_id`),
  KEY `optionvalue_id_refs_id_60509602` (`optionvalue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13324 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conservation_item_textblock_condition`
--

DROP TABLE IF EXISTS `conservation_item_textblock_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conservation_item_textblock_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `optionvalue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_id` (`item_id`,`optionvalue_id`),
  KEY `optionvalue_id_refs_id_3c885bce` (`optionvalue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18984 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conservation_item_textblock_material`
--

DROP TABLE IF EXISTS `conservation_item_textblock_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conservation_item_textblock_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `optionvalue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_id` (`item_id`,`optionvalue_id`),
  KEY `optionvalue_id_refs_id_677cd51d` (`optionvalue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10064 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conservation_optioncategory`
--

DROP TABLE IF EXISTS `conservation_optioncategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conservation_optioncategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conservation_optionvalue`
--

DROP TABLE IF EXISTS `conservation_optionvalue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conservation_optionvalue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `sort_order` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_id` (`category_id`,`text`),
  KEY `conservation_optionvalue_category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1322 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conservation_survey`
--

DROP TABLE IF EXISTS `conservation_survey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conservation_survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection` varchar(255) NOT NULL,
  `description` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `collection` (`collection`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_type`
--

DROP TABLE IF EXISTS `content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=872 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dbtrials_trial`
--

DROP TABLE IF EXISTS `dbtrials_trial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbtrials_trial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `url` varchar(200) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `consortium` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `description` longtext NOT NULL,
  `status` varchar(40) NOT NULL,
  `end_date` date DEFAULT NULL,
  `one_time_cost` decimal(9,2) DEFAULT NULL,
  `annual_cost` decimal(9,2) DEFAULT NULL,
  `annual_access_fee` decimal(9,2) DEFAULT NULL,
  `requested_by` varchar(40) NOT NULL,
  `notes` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=3736 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `directory_orgunit`
--

DROP TABLE IF EXISTS `directory_orgunit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_orgunit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `sap_org_unit` varchar(20) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `head_id` int(11) DEFAULT NULL,
  `head_title` varchar(255) NOT NULL,
  `is_admin_unit` tinyint(1) DEFAULT NULL,
  `campus_box` varchar(255) DEFAULT NULL,
  `physical_address` varchar(255) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `fax` varchar(40) DEFAULT NULL,
  `phone` varchar(40) DEFAULT NULL,
  `phone2` varchar(40) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `directory_orgunit_parent_id` (`parent_id`),
  KEY `directory_orgunit_head_id` (`head_id`),
  FULLTEXT KEY `name_fulltext` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=508 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `directory_orgunit_members`
--

DROP TABLE IF EXISTS `directory_orgunit_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_orgunit_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orgunit_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orgunit_id` (`orgunit_id`,`person_id`),
  KEY `person_id_refs_id_4a98c211` (`person_id`)
) ENGINE=MyISAM AUTO_INCREMENT=42622 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `directory_person`
--

DROP TABLE IF EXISTS `directory_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(40) NOT NULL,
  `middle_name` varchar(40) DEFAULT NULL,
  `last_name` varchar(40) NOT NULL,
  `nickname` varchar(40) DEFAULT NULL,
  `display_name` varchar(80) DEFAULT NULL,
  `net_id` varchar(20) NOT NULL,
  `unique_id` varchar(20) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `preferred_title` varchar(255) DEFAULT NULL,
  `is_eg_member` tinyint(1) DEFAULT NULL,
  `keywords` longtext,
  `photo_url` varchar(200) DEFAULT NULL,
  `ldap_dn` varchar(255) NOT NULL,
  `sap_org_unit` varchar(20) NOT NULL,
  `primary_affiliation` varchar(40) NOT NULL,
  `affiliation` varchar(255) NOT NULL,
  `campus_box` varchar(255) DEFAULT NULL,
  `physical_address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fax` varchar(40) DEFAULT NULL,
  `phone` varchar(40) DEFAULT NULL,
  `phone2` varchar(40) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `email_privacy` tinyint(1) DEFAULT NULL,
  `profile` longtext NOT NULL,
  `office_hours` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `net_id` (`net_id`),
  UNIQUE KEY `unique_id` (`unique_id`),
  UNIQUE KEY `ldap_dn` (`ldap_dn`),
  FULLTEXT KEY `display_name` (`display_name`),
  FULLTEXT KEY `first_name` (`first_name`),
  FULLTEXT KEY `last_name` (`last_name`),
  FULLTEXT KEY `name_fulltext` (`first_name`,`last_name`)
) ENGINE=MyISAM AUTO_INCREMENT=7824 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `directory_subjectguide`
--

DROP TABLE IF EXISTS `directory_subjectguide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_subjectguide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(200) NOT NULL,
  `order` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `directory_subjectguide_21b911c5` (`person_id`)
) ENGINE=MyISAM AUTO_INCREMENT=658 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_user_id` (`user_id`),
  KEY `django_admin_log_content_type_id` (`content_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=172223 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_comment_flags`
--

DROP TABLE IF EXISTS `django_comment_flags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_comment_flags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `flag_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`comment_id`,`flag`),
  KEY `django_comment_flags_user_id` (`user_id`),
  KEY `django_comment_flags_comment_id` (`comment_id`),
  KEY `django_comment_flags_flag` (`flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_comments`
--

DROP TABLE IF EXISTS `django_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_type_id` int(11) NOT NULL,
  `object_pk` longtext NOT NULL,
  `site_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_email` varchar(75) NOT NULL,
  `user_url` varchar(200) NOT NULL,
  `comment` longtext NOT NULL,
  `submit_date` datetime NOT NULL,
  `ip_address` char(15) DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL,
  `is_removed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_comments_content_type_id` (`content_type_id`),
  KEY `django_comments_site_id` (`site_id`),
  KEY `django_comments_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1856 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=872 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_favorites`
--

DROP TABLE IF EXISTS `django_favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stamp` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `object_repr` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_c25c2c28` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `easy_thumbnails_source`
--

DROP TABLE IF EXISTS `easy_thumbnails_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easy_thumbnails_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_hash` varchar(40) NOT NULL,
  `name` varchar(255) NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `storage_hash` (`storage_hash`,`name`),
  KEY `easy_thumbnails_source_3a997c55` (`storage_hash`),
  KEY `easy_thumbnails_source_52094d6e` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=872 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `easy_thumbnails_thumbnail`
--

DROP TABLE IF EXISTS `easy_thumbnails_thumbnail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easy_thumbnails_thumbnail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_hash` varchar(40) NOT NULL,
  `name` varchar(255) NOT NULL,
  `modified` datetime NOT NULL,
  `source_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `storage_hash` (`storage_hash`,`name`,`source_id`),
  KEY `easy_thumbnails_thumbnail_3a997c55` (`storage_hash`),
  KEY `easy_thumbnails_thumbnail_52094d6e` (`name`),
  KEY `easy_thumbnails_thumbnail_89f89e85` (`source_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7018 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stamp` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `object_repr` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `content_type_id` (`content_type_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `favorites_ibfk_1` FOREIGN KEY (`content_type_id`) REFERENCES `content_type` (`id`),
  CONSTRAINT `favorites_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floorplans_floorarea`
--

DROP TABLE IF EXISTS `floorplans_floorarea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floorplans_floorarea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coordinates` varchar(750) NOT NULL,
  `shape` varchar(100) NOT NULL,
  `floor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `floorplans_floorarea_df8d554f` (`floor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=302 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floorplans_floorplan`
--

DROP TABLE IF EXISTS `floorplans_floorplan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floorplans_floorplan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `floorplan_id` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `label_subtitle` varchar(255) DEFAULT NULL COMMENT 'Optional subtitle for special use-cases (i.e. donor names)',
  `building` varchar(15) NOT NULL,
  `map_image_full_size_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `floorplan_id` (`floorplan_id`),
  KEY `floorplans_floorplan_dbf3e5cd` (`map_image_full_size_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floorplans_floorplanmap`
--

DROP TABLE IF EXISTS `floorplans_floorplanmap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floorplans_floorplanmap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `label` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=316 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floorplans_room`
--

DROP TABLE IF EXISTS `floorplans_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floorplans_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` varchar(255) NOT NULL,
  `room_number` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `floorplan_id` int(11) NOT NULL,
  `naming_opportunity` tinyint(1) NOT NULL,
  `nameable` tinyint(1) NOT NULL,
  `dollar_amount` int(11) DEFAULT NULL,
  `pending_sale` tinyint(1) NOT NULL,
  `carrel` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `room_id` (`room_id`),
  KEY `floorplans_room_e008e8e2` (`floorplan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14670 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floorplans_room_area`
--

DROP TABLE IF EXISTS `floorplans_room_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floorplans_room_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coordinates` varchar(750) NOT NULL,
  `shape` varchar(100) NOT NULL,
  `room_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `floorplans_room_area_109d8a5f` (`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5192 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floorplans_room_mockup_images`
--

DROP TABLE IF EXISTS `floorplans_room_mockup_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floorplans_room_mockup_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `roommockup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `room_id` (`room_id`,`roommockup_id`),
  KEY `floorplans_room_mockup_images_109d8a5f` (`room_id`),
  KEY `floorplans_room_mockup_images_65895786` (`roommockup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2250 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floorplans_roommockup`
--

DROP TABLE IF EXISTS `floorplans_roommockup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floorplans_roommockup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `label` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=3092 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gifts_donor`
--

DROP TABLE IF EXISTS `gifts_donor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gifts_donor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `donor_type` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `category` varchar(40) NOT NULL,
  `title` varchar(40) NOT NULL,
  `first_name` varchar(40) NOT NULL,
  `middle_name` varchar(40) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `honors` varchar(255) NOT NULL,
  `address` longtext NOT NULL,
  `city` varchar(100) NOT NULL,
  `state_country` varchar(100) NOT NULL,
  `postal_code` varchar(20) NOT NULL,
  `digital_contact` varchar(255) NOT NULL,
  `notes` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105684 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gifts_gift`
--

DROP TABLE IF EXISTS `gifts_gift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gifts_gift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `donor_id` int(11) NOT NULL,
  `date_received` date DEFAULT NULL,
  `date_processed` date DEFAULT NULL,
  `date_receipt_sent` date DEFAULT NULL,
  `receipt_sent_by` varchar(50) NOT NULL,
  `gift_plate` tinyint(1) NOT NULL,
  `language_id` varchar(3) DEFAULT NULL,
  `appraised` tinyint(1) NOT NULL,
  `condition` varchar(255) NOT NULL,
  `estimated_value` decimal(9,2) DEFAULT NULL,
  `rbmscl` tinyint(1) NOT NULL,
  `subscription` tinyint(1) NOT NULL,
  `description` longtext NOT NULL,
  `notes` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gifts_gift_donor_id` (`donor_id`),
  KEY `gifts_gift_language_id` (`language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51838 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gifts_gift_subject`
--

DROP TABLE IF EXISTS `gifts_gift_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gifts_gift_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gift_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `gift_id` (`gift_id`,`subject_id`),
  KEY `subject_id_refs_id_213077df` (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13686 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gifts_giftformat`
--

DROP TABLE IF EXISTS `gifts_giftformat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gifts_giftformat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gifts_giftgroup`
--

DROP TABLE IF EXISTS `gifts_giftgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gifts_giftgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gift_id` int(11) NOT NULL,
  `format_id` int(11) NOT NULL,
  `items` int(10) unsigned NOT NULL,
  `notes` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `gift_id` (`gift_id`,`format_id`),
  KEY `gifts_giftgroup_gift_id` (`gift_id`),
  KEY `gifts_giftgroup_format_id` (`format_id`)
) ENGINE=InnoDB AUTO_INCREMENT=68446 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gifts_language`
--

DROP TABLE IF EXISTS `gifts_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gifts_language` (
  `code` varchar(3) NOT NULL,
  `value` varchar(255) NOT NULL,
  `in_pick_list` tinyint(1) NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `value` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gifts_subject`
--

DROP TABLE IF EXISTS `gifts_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gifts_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subject` (`subject`)
) ENGINE=InnoDB AUTO_INCREMENT=760 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `libguidemap_entry`
--

DROP TABLE IF EXISTS `libguidemap_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `libguidemap_entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_code` varchar(8) NOT NULL,
  `subject_desc` varchar(255) NOT NULL,
  `library` varchar(20) NOT NULL,
  `guide_url` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subject_code` (`subject_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2662 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locationguide_areamap`
--

DROP TABLE IF EXISTS `locationguide_areamap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locationguide_areamap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `building` varchar(10) NOT NULL,
  `area` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=390 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locationguide_collection`
--

DROP TABLE IF EXISTS `locationguide_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locationguide_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(8) NOT NULL,
  `label` varchar(255) NOT NULL,
  `sublibrary_id` int(11) DEFAULT NULL,
  `collection_group_id` int(11) DEFAULT NULL,
  `map_id` int(11) DEFAULT NULL,
  `external_link_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `locationguide_collection_sublibrary_id` (`sublibrary_id`),
  KEY `locationguide_collection_collection_group_id` (`collection_group_id`),
  CONSTRAINT `locationguide_collection_ibfk_1` FOREIGN KEY (`sublibrary_id`) REFERENCES `locationguide_sublibrary` (`id`),
  CONSTRAINT `locationguide_collection_ibfk_2` FOREIGN KEY (`collection_group_id`) REFERENCES `locationguide_collectiongroup` (`id`),
  CONSTRAINT `locationguide_collection_ibfk_3` FOREIGN KEY (`map_id`) REFERENCES `locationguide_areamap` (`id`),
  CONSTRAINT `locationguide_collection_ibfk_4` FOREIGN KEY (`external_link_id`) REFERENCES `locationguide_externallink` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8118 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locationguide_collectiongroup`
--

DROP TABLE IF EXISTS `locationguide_collectiongroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locationguide_collectiongroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sublibrary_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `locationguide_collectiongroup_sublibrary_id` (`sublibrary_id`),
  CONSTRAINT `locationguide_collectiongroup_ibfk_1` FOREIGN KEY (`sublibrary_id`) REFERENCES `locationguide_sublibrary` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=866 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locationguide_externallink`
--

DROP TABLE IF EXISTS `locationguide_externallink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locationguide_externallink` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link_text` varchar(255) NOT NULL,
  `generic_msg_id` int(11) DEFAULT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `locationguide_externallink_generic_msg_id` (`generic_msg_id`),
  CONSTRAINT `locationguide_externallink_ibfk_1` FOREIGN KEY (`generic_msg_id`) REFERENCES `locationguide_genericmessage` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=263 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locationguide_genericmessage`
--

DROP TABLE IF EXISTS `locationguide_genericmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locationguide_genericmessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(60) NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `label` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locationguide_locationmap`
--

DROP TABLE IF EXISTS `locationguide_locationmap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locationguide_locationmap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_group_id` int(11) NOT NULL,
  `map_id` int(11) DEFAULT NULL,
  `callno_range` varchar(100) NOT NULL,
  `callno_type` varchar(1) NOT NULL,
  `callno_start` varchar(100) NOT NULL,
  `callno_end` varchar(100) NOT NULL,
  `external_link_id` int(11) DEFAULT NULL,
  `map_desc` varchar(255) DEFAULT NULL,
  `callno_priority` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Used when collections share a call number range.',
  PRIMARY KEY (`id`),
  KEY `locationguide_locationmap_collection_group_id` (`collection_group_id`),
  KEY `locationguide_locationmap_map_id` (`map_id`),
  KEY `locationguide_locationmap_external_link_id` (`external_link_id`),
  CONSTRAINT `locationguide_locationmap_ibfk_1` FOREIGN KEY (`map_id`) REFERENCES `locationguide_areamap` (`id`),
  CONSTRAINT `locationguide_locationmap_ibfk_2` FOREIGN KEY (`external_link_id`) REFERENCES `locationguide_externallink` (`id`),
  CONSTRAINT `locationguide_locationmap_ibfk_3` FOREIGN KEY (`collection_group_id`) REFERENCES `locationguide_collectiongroup` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=841 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locationguide_sublibrary`
--

DROP TABLE IF EXISTS `locationguide_sublibrary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locationguide_sublibrary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(8) NOT NULL,
  `label` varchar(255) NOT NULL,
  `collgroup_choice` tinyint(1) NOT NULL,
  `default_locmap_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `default_locmap_id` (`default_locmap_id`),
  CONSTRAINT `locationguide_sublibrary_ibfk_1` FOREIGN KEY (`default_locmap_id`) REFERENCES `locationguide_locationmap` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=424 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbmscl_orders_order`
--

DROP TABLE IF EXISTS `rbmscl_orders_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbmscl_orders_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patron_id` int(11) NOT NULL,
  `invoice_date` date NOT NULL,
  `notes` longtext NOT NULL,
  `paid_by_cc` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rbmscl_orders_order_patron_id` (`patron_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11812 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbmscl_orders_order_services`
--

DROP TABLE IF EXISTS `rbmscl_orders_order_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbmscl_orders_order_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_id` (`order_id`,`service_id`),
  KEY `service_id_refs_id_4b76f580` (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14992 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbmscl_orders_patron`
--

DROP TABLE IF EXISTS `rbmscl_orders_patron`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbmscl_orders_patron` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(40) NOT NULL,
  `middle_name` varchar(40) NOT NULL,
  `last_name` varchar(40) NOT NULL,
  `email` varchar(75) NOT NULL,
  `phone` varchar(40) NOT NULL,
  `phone2` varchar(40) NOT NULL,
  `fax` varchar(40) NOT NULL,
  `address` longtext NOT NULL,
  `notes` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `last_name` (`last_name`,`first_name`,`middle_name`)
) ENGINE=InnoDB AUTO_INCREMENT=10512 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbmscl_orders_service`
--

DROP TABLE IF EXISTS `rbmscl_orders_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbmscl_orders_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=312 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-16  8:56:28
