module gitlab.oit.duke.edu/dul-go/locguide-app

go 1.13

require (
	github.com/crewjam/saml v0.4.5
	github.com/felixge/httpsnoop v1.0.2 // indirect
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	github.com/kelseyhightower/envconfig v1.4.0
	gitlab.oit.duke.edu/dul-go/httpresponse v0.3.2
	gitlab.oit.duke.edu/dul-go/journal v0.0.0-20190219194120-2a5b5450b878
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/driver/mysql v1.1.2
	gorm.io/gorm v1.21.13
)
