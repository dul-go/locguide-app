package model

import (
	"strconv"

	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
	"gorm.io/gorm"
)

type Sublibrary struct {
	// Inherits ID, CreateAt, UpdatedAt, DeletedAt
	gorm.Model

	Code             string `gorm:"type:varchar(8)"`
	Label            string `gorm:"type:varchar(255)"`
	CollGroupChoice  bool   `gorm:"column:collgroup_choice;type:tinyint(1);not null"`
	CollectionGroups []CollectionGroup

	LocationMapID int         `gorm:"column:default_locmap_id;type:int"`
	LocationMap   LocationMap `gorm:"foreignKey:LocationMapID"`
}

func (Sublibrary) TableName() string {
	return "locationguide_sublibrary"
}

func (s *Sublibrary) ScanForm(form locguide.ParamValues, prefix string) error {
	s.Code = form.Get(prefix + "code")
	s.Label = form.Get(prefix + "label")

	// process "checkbox" input by using strconv.ParseBool
	// make sure to handle the cases where either:
	// - the field is not part of the form submission, or
	// - the field is present but with a blank value
	var err error
	if s.CollGroupChoice, err = strconv.ParseBool(form.Get(prefix + "collgroup_choice")); err != nil {
		s.CollGroupChoice = false
	}

	s.LocationMapID, _ = strconv.Atoi(form.Get(prefix + "default_locmap_id"))

	return nil
}
