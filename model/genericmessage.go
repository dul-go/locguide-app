package model

import (
	"gitlab.oit.duke.edu/dul-go/journal"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
	"gorm.io/gorm"
)

//GenericMessage contains a label, content (string) and 0..N ExternalLinks
type GenericMessage struct {
	gorm.Model
	Label         string `gorm:"type:varchar(60)"`
	Content       string
	ExternalLinks []ExternalLink `gorm:"foreignKey:GenericMsgID"`
}

func (GenericMessage) TableName() string {
	return "locationguide_genericmessage"
}

func (g *GenericMessage) BeforeSave(tx *gorm.DB) (err error) {
	journal.Debug.Println("GenericMessage: BeforeSave")
	// add validations here, returning err if something is off
	// valid := true
	//if valid != true {
	//	err = errors.New("Can't save data right now. We're closed!")
	//}
	return
}

func (g *GenericMessage) ScanForm(form locguide.ParamValues, prefix string) error {
	g.Label = form.Get(prefix + "label")
	g.Content = form.Get(prefix + "content")

	return nil
}
