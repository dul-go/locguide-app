package model

import "gorm.io/gorm"

type UsesEntity interface {
	Entity() LgModel
}

type LgModel struct {
	// Inherit ID, CreatedAt, UpdatedAt, DeletedAt
	gorm.Model
}
