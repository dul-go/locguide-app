package model

import (
	"strconv"

	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
	"gorm.io/gorm"
)

type Collection struct {
	// Inherits ID, CreatedAt, UpdatedAt, DeletedAt
	gorm.Model

	Code  string `gorm:"type:varchar(8)"`
	Label string `gorm:"type:varchar(255)"`

	SublibraryID *int        `gorm:"type:int"`
	Sublibrary   *Sublibrary `gorm:"foreignKey:SublibraryID"`

	CollectionGroupID *int             `gorm:"type:int"`
	CollectionGroup   *CollectionGroup `gorm:"foreignKey:CollectionGroupID"`
}

func (Collection) TableName() string {
	return "locationguide_collection"
}

func (c *Collection) ScanForm(form locguide.ParamValues, prefix string) error {
	c.Code = form.Get(prefix + "code")
	c.Label = form.Get(prefix + "label")

	if sID := form.Get(prefix + "sublibrary_id"); len(sID) > 0 {
		c.SublibraryID = new(int)
		*c.SublibraryID, _ = strconv.Atoi(sID)
	} else {
		c.SublibraryID = nil
		c.Sublibrary = &Sublibrary{}
	}

	if cgID := form.Get(prefix + "collection_group_id"); len(cgID) > 0 {
		c.CollectionGroupID = new(int)
		*c.CollectionGroupID, _ = strconv.Atoi(cgID)
	} else {
		c.CollectionGroupID = nil
		c.CollectionGroup = &CollectionGroup{}
	}

	return nil
}
