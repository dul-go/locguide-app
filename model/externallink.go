package model

import (
	"fmt"
	"strconv"

	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
	"gorm.io/gorm"
)

//ExternalLink descibes an external link
type ExternalLink struct {
	gorm.Model
	Url            string
	Title          string         `gorm:"type:varchar(255)"`
	LinkText       string         `gorm:"size:255"`
	GenericMsgID   *int           `gorm:"type:int"`
	GenericMessage GenericMessage `gorm:"foreignKey:GenericMsgID"`
	Content        string
}

func (ExternalLink) TableName() string {
	return "locationguide_externallink"
}

func (e ExternalLink) String() string {
	return fmt.Sprintf("ID: %d | Title: %s | LinkText: %s | GenericMsgID: %d | GenericMessage: %v\n", e.ID, e.Title, e.LinkText, e.GenericMessage.ID, e.GenericMessage)
}

func (e *ExternalLink) ScanForm(form locguide.ParamValues, prefix string) error {
	e.Content = form.Get(prefix + "content")
	e.LinkText = form.Get(prefix + "link_text")
	e.Title = form.Get(prefix + "title")
	e.Url = form.Get(prefix + "url")

	if gID := form.Get(prefix + "generic_msg_id"); len(gID) > 0 {
		e.GenericMsgID = new(int)
		*e.GenericMsgID, _ = strconv.Atoi(gID)
	} else {
		e.GenericMsgID = nil
		e.GenericMessage = GenericMessage{}
	}

	return nil
}
