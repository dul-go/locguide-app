package model

import (
	"strconv"

	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
)

type CollectionGroup struct {
	// Inherits ID, CreateAt, UpdatedAt, DeletedAt
	LgModel

	Description string `gorm:"type:varchar(255)"`

	SublibraryID int         `gorm:"type:int"`
	Sublibrary   *Sublibrary `gorm:"foreignKey:SublibraryID"`
}

func (CollectionGroup) TableName() string {
	return "locationguide_collectiongroup"
}

func (c *CollectionGroup) ScanForm(form locguide.ParamValues, prefix string) error {
	c.Description = form.Get(prefix + "description")
	c.SublibraryID, _ = strconv.Atoi(form.Get(prefix + "sublibrary_id"))

	return nil
}
