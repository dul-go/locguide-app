package model

import (
	"gitlab.oit.duke.edu/dul-go/journal"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
	"gorm.io/gorm"
)

type AreaMap struct {
	gorm.Model
	Building string `gorm:"type:varchar(10)"`
	Area     string `gorm:"type:varchar(255)"`
	Content  string `gorm:"type:varchar(100)"`
	Image    string `gorm:"type:varchar(100)"`
}

func (AreaMap) TableName() string {
	return "locationguide_areamap"
}

// BeforeSave implements gorm transaction hook
func (a *AreaMap) BeforeSave(tx *gorm.DB) (err error) {
	journal.Debug.Println("AreaMap: BeforeSave")
	return
}

func (a *AreaMap) ScanForm(form locguide.ParamValues, prefix string) error {
	a.Building = form.Get(prefix + "building")
	a.Area = form.Get(prefix + "area")
	a.Image = form.Get(prefix + "image")

	return nil
}
