package model

import (
	"fmt"
	"strconv"

	"gitlab.oit.duke.edu/dul-go/journal"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
	"gorm.io/gorm"
)

type LocationMap struct {
	// Inherits ID, CreatedAt, UpdatedAt, DeletedAt
	//gorm.Model
	LgModel

	CollectionGroupID *int            `gorm:"type:int;default:NULL"`
	CollectionGroup   CollectionGroup `gorm:"foreignKey:CollectionGroupID;constraint:OnUpdate:NO ACTION"`

	CallNumberRange    string `gorm:"column:callno_range;type:varchar(100)"`
	CallNumberStart    string `gorm:"column:callno_start;type:varchar(100)"`
	CallNumberEnd      string `gorm:"column:callno_end;type:varchar(100)"`
	CallNumberType     string `gorm:"column:callno_type;type:varchar(1)"`
	CallNumberPriority int    `gorm:"column:callno_priority;type:tinyint(4)"`

	MapDescription string `gorm:"column:map_desc;type:varchar(255)"`

	ExternalLinkID *int         `gorm:"type:int;default:NULL"`
	ExternalLink   ExternalLink `gorm:"foreignKey:ExternalLinkID;constraint:OnUpdate:NO ACTION"`

	AreaMapID *int    `gorm:"column:map_id;type:int;default:NULL"`
	AreaMap   AreaMap `gorm:"foreignKey:AreaMapID;constraint:OnUpdate:NO ACTION"`
}

func (LocationMap) TableName() string {
	return "locationguide_locationmap"
}

func (l LocationMap) String() string {
	return fmt.Sprintf("ID: %d | AreaMapID: %v | ExternalLinkID: %v", l.ID, l.AreaMapID, l.ExternalLinkID)
}

func (l *LocationMap) BeforeSave(tx *gorm.DB) (err error) {
	return
}

func (l *LocationMap) AfterSave(tx *gorm.DB) (err error) {
	return
}

func (l *LocationMap) ScanForm(form locguide.ParamValues, prefix string) error {
	l.CallNumberRange = form.Get(prefix + "call_number_range")
	l.CallNumberStart = form.Get(prefix + "call_number_start")
	l.CallNumberEnd = form.Get(prefix + "call_number_end")
	l.CallNumberPriority, _ = strconv.Atoi(form.Get(prefix + "call_number_priority"))

	if eID := form.Get(prefix + "external_link_id"); len(eID) > 0 {
		l.ExternalLinkID = new(int)
		*l.ExternalLinkID, _ = strconv.Atoi(eID)
	} else {
		l.ExternalLinkID = nil
		l.ExternalLink = ExternalLink{}
	}

	if cgID := form.Get(prefix + "collection_group_id"); len(cgID) > 0 {
		l.CollectionGroupID = new(int)
		*l.CollectionGroupID, _ = strconv.Atoi(cgID)
	} else {
		l.CollectionGroupID = nil
		l.CollectionGroup = CollectionGroup{}
	}

	if aID := form.Get(prefix + "map_id"); len(aID) > 0 {
		l.AreaMapID = new(int)
		*l.AreaMapID, _ = strconv.Atoi(aID)
	} else {
		l.AreaMapID = nil
		l.AreaMap = AreaMap{}
	}
	journal.Debug.Printf("%s\n\n", l)

	return nil
}
