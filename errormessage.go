package main

import "text/template"

type ErrorMessageBox struct {
	Content  []byte
	tmplName string
}

func NewErrorMessageBox(tmplName string) *ErrorMessageBox {
	return &ErrorMessageBox{
		Content:  []byte(""),
		tmplName: tmplName,
	}
}

func (b *ErrorMessageBox) Render(data map[string]interface{}) error {
	t := template.Must(template.ParseFiles(b.tmplName))
	err := t.Execute(b, data)
	return err
}

func (b *ErrorMessageBox) Write(buf []byte) (int, error) {
	b.Content = append(b.Content, buf...)
	return 0, nil
}
