package main

import (
	"fmt"
	"text/template"

	"gitlab.oit.duke.edu/dul-go/httpresponse"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
	"gitlab.oit.duke.edu/dul-go/locguide-app/model"
)

// Verify (at runtime) that GenericMessageController{} implements
// locguide.Controller
var _ Controller = &GenericMessageController{}

type GenericMessageController struct{}

func (g *GenericMessageController) PathPrefix() string {
	return "/genericmessages/"
}

func (g *GenericMessageController) Listing(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	var genericMessages []model.GenericMessage
	err := svc.FindThese(&genericMessages)

	if err != nil {
		return nil, fmt.Errorf("Listing: unable to find generic messages")
	}

	deleteModal := NewDeleteModal("tmpl/delete_modal.go.html", c)
	if err = deleteModal.Render(c.TemplateData()); err != nil {
		panic(err)
	}
	c.AddTemplateData("DeleteModal", deleteModal.Modal)
	c.AddTemplateData("GenericMessages", genericMessages)

	pageDocument := &httpresponse.PageDocument{Title: "Generic Messages", Body: []byte("")}
	bodyTemplate := template.Must(template.ParseFiles("tmpl/genericmessages.go.html"))
	err = bodyTemplate.Execute(pageDocument, c.TemplateData())
	if err != nil {
		return nil, fmt.Errorf("Listing (Generic Messages): unable to execute template - %v", err)
	}

	return pageDocument, nil
}

func (g *GenericMessageController) Form(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	var genericMessage model.GenericMessage

	err := svc.FirstOrInit(&genericMessage, c.IdForRequestedResource())
	if err != nil {
		return nil, fmt.Errorf("GenericMessageForm: Unable to load generic message (%v)", err)
	}

	if c.IsPostRequest() {
		// save data from the form
		genericMessage.ScanForm(c.Params(), "")
		err := svc.Save(&genericMessage)
		if err != nil {
			return nil, fmt.Errorf("GenericMessageForm error: %v", err)
		}

		c.SetSessionKey("success_message", "Changes have been saved.")
		c.Redirect(fmt.Sprintf("%s%d", g.PathPrefix(), genericMessage.ID), 303)
	}

	c.AddTemplateData("GenericMessage", genericMessage)
	c.AddTemplateData("Entity", genericMessage)

	formButtons := NewFormButtons("tmpl/form_buttons.go.html", c)
	if err = formButtons.Render(c.TemplateData()); err != nil {
		panic(err)
	}
	c.AddTemplateData("FormButtons", formButtons.Buttons)

	pageDocument := &httpresponse.PageDocument{Title: "Generic Message", Body: []byte("")}
	bodyTemplate := template.Must(template.ParseFiles("tmpl/genericmessage.go.html"))
	err = bodyTemplate.Execute(pageDocument, c.TemplateData())
	if err != nil {
		return nil, fmt.Errorf("GenericMessageForm: unable to execute template - %v", err)
	}

	return pageDocument, nil
}

// Delete (or DeleteHandler) will facilitate removing
// a GenericMessage entity
func (g *GenericMessageController) DeleteHandler(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	err := svc.Delete(&model.GenericMessage{}, c.IdForRequestedResource())
	if err != nil {
		return nil, fmt.Errorf("GenericMessage Delete: unable to delete generic message - %v\n", err)
	}

	c.SetSessionKey("success_message", "The selected generic message has been deleted.")
	c.Redirect(fmt.Sprintf("%s", g.PathPrefix()), 303)

	pageDocument := &httpresponse.PageDocument{Title: "Delete Generic Message", Body: []byte("")}
	return pageDocument, nil
}
