package main

import (
	"gitlab.oit.duke.edu/dul-go/httpresponse"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
)

type Controller interface {
	PathPrefix() string
	Listing(c locguide.Context) (*httpresponse.PageDocument, error)
	Form(c locguide.Context) (*httpresponse.PageDocument, error)
	DeleteHandler(c locguide.Context) (*httpresponse.PageDocument, error)
}
