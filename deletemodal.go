package main

import (
	"text/template"

	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
)

type DeleteModal struct {
	Modal []byte

	tmplName string

	ReturnPath string
}

func NewDeleteModal(tmplName string, ctx locguide.Context) *DeleteModal {
	dm := DeleteModal{}
	dm.Modal = []byte("")
	dm.tmplName = tmplName
	dm.ReturnPath = ctx.Request().Referer()
	return &dm
}

func (d *DeleteModal) Render(data map[string]interface{}) error {
	t := template.Must(template.ParseFiles(d.tmplName))
	data["ReturnPath"] = d.ReturnPath
	err := t.Execute(d, data)
	return err
}

func (d *DeleteModal) Write(buf []byte) (int, error) {
	d.Modal = append(d.Modal, buf...)
	return 0, nil
}
