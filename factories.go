package main

import (
	"net/http"
	"net/url"
	"sync"

	"github.com/gorilla/mux"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
)

func (a *App) NewContext(res http.ResponseWriter, req *http.Request) locguide.Context {
	data := &sync.Map{}

	data.Store("app", a)
	data.Store("current_path", req.URL.Path)
	data.Store("method", req.Method)
	data.Store("config", a.config)
	data.Store("referer", req.Referer())
	data.Store("current_path", req.URL.Path)

	// Parse URL Params
	params := url.Values{}
	vars := mux.Vars(req)
	for k, v := range vars {
		params.Add(k, v)
	}

	// Parse URL Query String Params
	// For POST, PUT, and PATCH requests, it also parse the request body as a form.
	// Request body parameters take precedence over URL query string values in params
	if err := req.ParseForm(); err == nil {
		for k, v := range req.Form {
			for _, vv := range v {
				params.Add(k, vv)
			}
		}
	}

	templateData := make(map[string]interface{})
	templateData["success_message"] = ""
	templateData["error_message"] = ""

	_, ok := a.session.Values["success_message"]
	if ok {
		templateData["success_message"] = a.session.Values["success_message"]
	}

	_, ok = a.session.Values["error_message"]
	if ok {
		templateData["error_message"] = a.session.Values["error_messasge"]
	}

	templateData["AppTitle"] = a.config.GetAppTitle()
	templateData["HasError"] = false
	templateData["UI"] = a.config.UI
	templateData["AuthDisplayName"] = false

	return &DefaultContext{
		Context:      req.Context(),
		response:     res,
		request:      req,
		data:         data,
		params:       params,
		session:      a.session,
		templateData: templateData,
	}
}
