package main

import (
	"gitlab.oit.duke.edu/dul-go/httpresponse"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
)

// DEPRECATED IDEA
type Handler struct{}

type PageHandler func(c locguide.Context) (*httpresponse.PageDocument, error)
