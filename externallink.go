package main

import (
	"fmt"
	"text/template"

	"gitlab.oit.duke.edu/dul-go/httpresponse"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
	"gitlab.oit.duke.edu/dul-go/locguide-app/model"
)

var _ Controller = &ExternalLinkController{}

type ExternalLinkController struct{}

func (e *ExternalLinkController) PathPrefix() string {
	return "/externallinks/"
}

func (e *ExternalLinkController) Listing(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	var externalLinks []model.ExternalLink
	pager := NewPagination(model.ExternalLink{}, app.config)
	err := svc.
		Paginate(pager, c.Params()).
		FindThese(&externalLinks)

	if err != nil {
		return nil, fmt.Errorf("Listing: unable to find external links")
	}

	// the resulting HTML for this render can be
	// accessed by pager.Content
	pager.Render(map[string]interface{}{
		"WorkingURL": c.Request().URL.Path,
		"Pager":      pager,
	})
	c.AddTemplateData("Pager", pager.Content)

	pageDocument := &httpresponse.PageDocument{Title: "External Links", Body: []byte("")}
	bodyTemplate := template.Must(template.ParseFiles("tmpl/externallinks.go.html"))
	err = bodyTemplate.Execute(pageDocument, externalLinks)
	if err != nil {
		return nil, fmt.Errorf("Listing (ExternalLinks): unable to execute template - %v", err)
	}

	return pageDocument, nil
}

func (e *ExternalLinkController) Form(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	var externalLink model.ExternalLink
	err := svc.FirstOrInit(&externalLink, c.IdForRequestedResource())
	if err != nil {
		return nil, fmt.Errorf("ExternalLinkForm: unable to load External Link - %v", err)
	}

	if c.IsPostRequest() {
		// save data from the form
		externalLink.ScanForm(c.Params(), "")
		err := svc.Save(&externalLink)
		if err != nil {
			return nil, fmt.Errorf("Form (ExternalLink): unable to save External Link - %v", err)
		}

		c.SetSessionKey("success_message", "Changes have been saved")
		c.Redirect(fmt.Sprintf("%s%d", e.PathPrefix(), externalLink.ID), 303)
		return &httpresponse.PageDocument{}, nil
	}

	c.AddTemplateData("ExternalLink", externalLink)
	c.AddTemplateData("Entity", externalLink)

	if genericMessages, err := svc.GenericMessages(); err == nil {
		c.AddTemplateData("GenericMessages", genericMessages)
	}

	formButtons := NewFormButtons("tmpl/form_buttons.go.html", c)
	if err = formButtons.Render(c.TemplateData()); err != nil {
		panic(err)
	}
	c.AddTemplateData("FormButtons", formButtons.Buttons)

	page_title := "External Link / " + externalLink.Title

	pageDocument := &httpresponse.PageDocument{Title: page_title, Body: []byte("")}
	bodyTemplate := template.Must(template.ParseFiles("tmpl/externallink.go.html"))
	err = bodyTemplate.Execute(pageDocument, c.TemplateData())
	if err != nil {
		return nil, fmt.Errorf("Form (ExternalLink): unable to execute template:<br />\n %v", err)

	}

	return pageDocument, nil
}

// Delete (or DeleteHandler) will facilitate removing
// a GenericMessage entity
func (e *ExternalLinkController) DeleteHandler(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	err := svc.Delete(&model.ExternalLink{}, c.IdForRequestedResource())
	if err != nil {
		return nil, fmt.Errorf("ExternalLink Delete: unable to delete external link - %v\n", err)
	}

	c.SetSessionKey("success_message", "The selected external link has been deleted.")
	c.Redirect(fmt.Sprintf("%s", e.PathPrefix()), 303)

	pageDocument := &httpresponse.PageDocument{Title: "Delete External Link", Body: []byte("")}
	return pageDocument, nil
}
