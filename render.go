package main

// Renderer interface provides one method
// Render with a "map" as input
type Renderable interface {
	Render(map[string]interface{})
}
