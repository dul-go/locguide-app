package main

import (
	"math"
	"net/url"
	"strconv"

	"gitlab.oit.duke.edu/dul-go/journal"
	"gitlab.oit.duke.edu/dul-go/locguide-app/config"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
	"gitlab.oit.duke.edu/dul-go/locguide-app/model"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const (
	PreloadHasMany   = true
	PreloadBelongsTo = true
	NoPreload        = false
)

type PagerFunc func(db *gorm.DB) *gorm.DB

var NoPager = func(db *gorm.DB) *gorm.DB {
	return db
}

type DatabaseService struct {
	db *gorm.DB

	// pager func used for pagination support
	config config.Config

	pagerFunc PagerFunc
}

// NewDatabaseService will open a new db connection
// specific to this application.
// Panic on db open error.
func NewDatabaseService(c config.Config) (*DatabaseService, error) {
	service := DatabaseService{config: c}
	db, err := gorm.Open(mysql.Open(c.GetDatabaseDSN()), &gorm.Config{})
	if err != nil {
		journal.Error.Println(err)
		return nil, err
	}
	journal.Debug.Println("database connection established!")

	service.pagerFunc = NoPager

	service.db = db
	return &service, nil
}

func (s *DatabaseService) RunAutoMigrations() error {
	// I believe 'AutoMigrate' is a separate story for a different
	// controller (and route) or application
	s.db.AutoMigrate(
		&model.ExternalLink{},
		&model.GenericMessage{},
		&model.AreaMap{},
		&model.Sublibrary{},
		&model.CollectionGroup{},
		&model.Collection{},
		&model.LocationMap{})

	return nil
}

// DB returns the active database connection
// to be used for operations not defined below.
// It's preferred to add functions here based on common patterns.
func (s *DatabaseService) DB() *gorm.DB {
	return s.db
}

// FirstOrInit provides a wrapper around GORM's FirstOrInit
func (s *DatabaseService) FirstOrInit(e interface{}, id string) error {
	err := s.db.Preload(clause.Associations).FirstOrInit(e, id).Error
	//journal.Debug.Printf("%v\n", e)
	return err
}

// FindThese calls gorm.Find(...) to load model instances
func (s *DatabaseService) FindThese(results interface{}, filters ...interface{}) error {
	result := s.db.Scopes(s.pagerFunc).Preload(clause.Associations).Find(results)
	//journal.Debug.Printf("rows affected: %d", result.RowsAffected)

	err := result.Error
	if err != nil {
		return err
	}
	return nil
}

func (s *DatabaseService) Paginate(pager *Pagination, p locguide.ParamValues) *DatabaseService {
	var totalRows int64
	s.db.Model(pager.value).Count(&totalRows)
	pager.TotalRows = totalRows

	pageQueryName := s.config.Database.Pager.PageQueryName

	page, _ := strconv.Atoi(p.Get(pageQueryName))
	if page == 0 {
		page = 1
	}
	pager.Page = page

	pageSize, _ := strconv.Atoi(p.Get("page_size"))
	switch {
	case pageSize > 100:
		pageSize = 100
	case pageSize <= 0:
		// TODO create a config for 'pageSize'
		pageSize = 20
	}
	pager.Limit = pageSize

	totalPages := int(math.Ceil(float64(totalRows) / float64(pager.Limit)))
	pager.TotalPages = totalPages
	if totalPages > page {
		pager.NextPage = page + 1
	}
	if page > 1 {
		pager.PreviousPage = page - 1
	}

	r := (page - 1) / pager.PagesPerChapter
	pager.CurrentChapter = r + 1
	pager.Begin = pager.PagesPerChapter*r + 1
	pager.PrevChapterEnd = pager.Begin - 1
	pager.End = int(math.Min(float64(pager.PagesPerChapter)*(float64(r)+float64(1)), float64(totalPages)))
	pager.NextChapterBegin = pager.End + 1
	pager.PageRange = NewSlice(pager.Begin, pager.End, 1)

	s.pagerFunc = func(db *gorm.DB) *gorm.DB {
		offset := (page - 1) * pageSize
		return db.Offset(offset).Limit(pageSize)
	}

	return s
}

// Save provides a wrapper to gormDb.Save
func (s *DatabaseService) Save(e interface{}) error {
	err := s.db.Save(e).Error
	if err != nil {
		return err
	}
	return nil
}

// Delete provides a wrapper to gormDb.Delete
func (s *DatabaseService) Delete(e interface{}, id string) error {
	err := s.db.Delete(e, id).Error
	if err != nil {
		return err
	}
	return nil
}

type ActiveBuilding struct {
	Building string
	Floors   []struct{ Floor string }
}

func (s *DatabaseService) ActiveBuildings() ([]model.AreaMap, error) {
	//var buildings []string
	//err := s.db.Model(&model.AreaMap{}).Distinct().Pluck("Building", &buildings).Error
	var areamaps []model.AreaMap
	err := s.db.Model(&model.AreaMap{}).
		Preload(clause.Associations).
		Distinct("Building").Pluck("Building", &areamaps).
		Error
	if err != nil {
		return nil, err
	}
	return areamaps, nil
}

// ActiveBuildingsWithMaps
func (s *DatabaseService) ActiveBuildings2() (map[string][]model.AreaMap, error) {
	buildings := make(map[string][]model.AreaMap)

	var areamaps []model.AreaMap
	err := s.db.Find(&areamaps).Error
	if err != nil {
		return nil, err
	}
	for _, s := range areamaps {
		if _, ok := buildings[s.Building]; !ok {
			buildings[s.Building] = []model.AreaMap{}
		}
		buildings[s.Building] = append(buildings[s.Building], s)
	}
	return buildings, nil
}

func (s *DatabaseService) AreaMaps(filters ...map[string]interface{}) ([]model.AreaMap, error) {
	areaMaps := []model.AreaMap{}
	err := s.FindThese(&areaMaps, filters)
	return areaMaps, err
}

func (s *DatabaseService) ExternalLinks(filters ...map[string]interface{}) ([]model.ExternalLink, error) {
	externalLinks := []model.ExternalLink{}
	err := s.FindThese(&externalLinks, filters)
	return externalLinks, err
}

func (s *DatabaseService) GenericMessages(filters ...map[string]interface{}) ([]model.GenericMessage, error) {
	genericMessages := []model.GenericMessage{}
	err := s.FindThese(&genericMessages, filters)
	return genericMessages, err
}

func (s *DatabaseService) CollectionGroups(filters ...map[string]interface{}) ([]model.CollectionGroup, error) {
	collectionGroups := []model.CollectionGroup{}
	err := s.FindThese(&collectionGroups, filters)
	return collectionGroups, err
}

func (s *DatabaseService) TakeValuesFromForm(e interface{}, form url.Values, prefix string) error {
	return nil
}
