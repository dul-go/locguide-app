package main

import (
	"fmt"
	"text/template"

	"gitlab.oit.duke.edu/dul-go/httpresponse"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
	"gitlab.oit.duke.edu/dul-go/locguide-app/model"
)

var _ Controller = &CollectionController{}

type CollectionController struct{}

func (c *CollectionController) PathPrefix() string {
	return "/collections/"
}

func (cr *CollectionController) Listing(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	var collections []model.Collection
	pager := NewPagination(model.Collection{}, app.config)
	err := svc.Paginate(pager, c.Params()).FindThese(&collections)

	if err != nil {
		return nil, fmt.Errorf("Listing: unable to find collections")
	}

	pager.Render(map[string]interface{}{
		"WorkingURL": c.Request().URL.Path,
		"Pager":      pager,
	})
	c.AddTemplateData("Pager", pager.Content)

	pageDocument := &httpresponse.PageDocument{Title: "Collections Listing", Body: []byte("")}
	bodyTemplate := template.Must(template.ParseFiles("tmpl/collections.go.html"))
	err = bodyTemplate.Execute(pageDocument, collections)
	if err != nil {
		return nil, fmt.Errorf("Listing (Collections): unable to execute template - %v", err)
	}

	return pageDocument, nil
}

func (cr *CollectionController) Form(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	var collection model.Collection
	err := svc.FirstOrInit(&collection, c.IdForRequestedResource())
	if err != nil {
		return nil, fmt.Errorf("Collection Form: unable to load collection record - %v", err)
	}

	if c.IsPostRequest() {
		collection.ScanForm(c.Params(), "")
		err := svc.Save(&collection)
		if err != nil {
			return nil, fmt.Errorf("Form (Collection): unable to save collection - %v", err)
		}

		c.SetSessionKey("success_message", "Changes have been saved")
		c.Redirect(fmt.Sprintf("%s%d", cr.PathPrefix(), collection.ID), 303)
	}

	c.AddTemplateData("Collection", collection)
	c.AddTemplateData("Entity", collection)

	formButtons := NewFormButtons("tmpl/form_buttons.go.html", c)
	if err = formButtons.Render(c.TemplateData()); err != nil {
		panic(err)
	}
	c.AddTemplateData("FormButtons", formButtons.Buttons)

	pageTitle := "Collection / " + collection.Code
	pageDocument := &httpresponse.PageDocument{Title: pageTitle, Body: []byte("")}
	bodyTemplate := template.Must(template.ParseFiles("tmpl/collection.go.html"))
	err = bodyTemplate.Execute(pageDocument, c.TemplateData())
	if err != nil {
		return nil, fmt.Errorf("Form (Collection): unable to execute template:<br />\n %v", err)
	}

	return pageDocument, nil
}

func (cr *CollectionController) DeleteHandler(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	err := svc.Delete(&model.Collection{}, c.IdForRequestedResource())
	if err != nil {
		return nil, fmt.Errorf("Collection Delete: unable to delete collection - %v\n", err)
	}

	c.SetSessionKey("success_message", "The selected collection has been deleted.")
	c.Redirect(fmt.Sprintf("%s", cr.PathPrefix()), 303)

	pageDocument := &httpresponse.PageDocument{Title: "Delete Collection", Body: []byte("")}
	return pageDocument, nil
}
