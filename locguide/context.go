package locguide

import (
	"context"
	"net/http"

	"github.com/gorilla/sessions"
)

type Context interface {
	context.Context
	Response() http.ResponseWriter
	Request() *http.Request
	Redirect(string, int)
	WillRedirect() bool
	WantsExistingResource() bool
	IdForRequestedResource() string
	Data() map[string]interface{}
	Set(string, interface{})
	Param(string) string
	Params() ParamValues
	IsPostRequest() bool
	IsGetRequest() bool
	Session() *sessions.Session
	SessKeyExists(string) bool
	SetSessionKey(string, interface{})
	SessionValueForKey(string) (interface{}, error)
	SessStringForKey(string) string
	SessBoolForKey(string) bool
	AddTemplateData(string, interface{})
	TemplateData() map[string]interface{}
	File(string) (File, error)

	// Ultimately, I may have to bite the bullet
	// and incorporate the "Buffalo" framework
	// because it does a great job of handling sessions
	SaveSession()
}

// Borrowed from Buffalo's context
// ParamValues will most commonly be url.Values
// but great to set our own :)
type ParamValues interface {
	Get(string) string
}
