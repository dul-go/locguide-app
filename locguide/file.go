package locguide

import "mime/multipart"

type File struct {
	multipart.File
	*multipart.FileHeader
}

func (f File) String() string {
	if f.File == nil {
		return ""
	}
	return f.Filename
}
