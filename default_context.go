package main

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"sync"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"gitlab.oit.duke.edu/dul-go/journal"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
)

var _ locguide.Context = &DefaultContext{}
var _ context.Context = &DefaultContext{}

type DefaultContext struct {
	context.Context
	data         *sync.Map
	response     http.ResponseWriter
	request      *http.Request
	params       url.Values
	session      *sessions.Session
	templateData map[string]interface{}
}

func (d *DefaultContext) AddTemplateData(k string, v interface{}) {
	d.templateData[k] = v
}

func (d *DefaultContext) TemplateData() map[string]interface{} {
	d.templateData["ContextData"] = d.Data()
	return d.templateData
}

// Set a value onto the Context. Any value set onto the Context
// will be automatically available in templates.
func (d *DefaultContext) Set(key string, value interface{}) {
	d.data.Store(key, value)
}

// Value that has previously stored on the context.
func (d *DefaultContext) Value(key interface{}) interface{} {
	if k, ok := key.(string); ok {
		if v, ok := d.data.Load(k); ok {
			return v
		}
	}
	return d.Context.Value(key)
}

func (d *DefaultContext) Session() *sessions.Session {
	return d.session
}

func (d *DefaultContext) SessKeyExists(k string) bool {
	_, ok := d.session.Values[k]
	return ok
}

func (d *DefaultContext) SetSessionKey(s string, data interface{}) {
	d.session.Values[s] = data
}

func (d *DefaultContext) SessionValueForKey(s string) (interface{}, error) {
	val, ok := d.session.Values[s]

	if !ok {
		return nil, fmt.Errorf("SessionValueForKey: unable to get value for key [%s]\n", s)
	}
	delete(d.session.Values, s)
	return val, nil
}

func (d *DefaultContext) SessStringForKey(s string) string {
	val, ok := d.SessionValueForKey(s)
	if ok != nil {
		return ""
	}
	return val.(string)
}

func (d *DefaultContext) SessBoolForKey(s string) bool {
	val, err := d.SessionValueForKey(s)
	if err != nil {
		return false
	}
	return val.(bool)
}

func (d *DefaultContext) SaveSession() {
	d.session.Save(d.request, d.response)
}

func (d *DefaultContext) Request() *http.Request {
	return d.request
}

func (d *DefaultContext) Response() http.ResponseWriter {
	return d.response
}

func (d *DefaultContext) Redirect(where string, statusCode int) {
	// Save all changes to the session first,
	// so messages (like 'success_message') don't get lost
	d.SaveSession()
	journal.Debug.Printf("Session after saving, prior to redirect: %v\n", d.session)
	http.Redirect(d.response, d.request, where, statusCode)
}

func (d *DefaultContext) WillRedirect() bool {
	_, ok := d.response.Header()["location"]
	return !ok
}

// Params returns all of the parameters for the request,
// including both named params and query string parameters.
func (d *DefaultContext) Params() locguide.ParamValues {
	return d.params
}

// Param returns a param, either named or query string,
// based on the key.
func (d *DefaultContext) Param(key string) string {
	return d.Params().Get(key)
}

// WantsExistingResource is a convienence function
// to signal if the request includes a named parameter, "id"
func (d *DefaultContext) WantsExistingResource() bool {
	vars := mux.Vars(d.request)
	_, ok := vars["id"]
	return ok
}

// IdForResource returns the value of the "id" named
// param from the route.
// If the value is not present, return string of "-1000"
func (d *DefaultContext) IdForRequestedResource() string {
	vars := mux.Vars(d.request)
	if id, ok := vars["id"]; ok {
		return id
	}
	return "-1000"

}

// Data contains all the values set through Get/Set
// borrowed (ok, ripped) from Buffalo's DefaultContext
func (d *DefaultContext) Data() map[string]interface{} {
	m := map[string]interface{}{}
	d.data.Range(func(k, v interface{}) bool {
		s, ok := k.(string)
		if !ok {
			return false
		}
		m[s] = v
		return true
	})
	return m
}

func (d *DefaultContext) IsPostRequest() bool {
	return d.request.Method == "POST"
}

func (d *DefaultContext) IsGetRequest() bool {
	return d.request.Method == "GET"
}

func (d *DefaultContext) File(name string) (locguide.File, error) {
	req := d.Request()
	if err := req.ParseMultipartForm(5 * 1024 * 1024); err != nil {
		return locguide.File{}, err
	}
	f, h, err := req.FormFile(name)
	fb := locguide.File{
		File:       f,
		FileHeader: h,
	}
	if err != nil {
		return fb, err
	}
	return fb, nil
}
