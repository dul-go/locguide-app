package main

import (
	"text/template"

	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
)

type FormButtons struct {
	Buttons []byte

	tmplName string

	ReturnPath string
}

func NewFormButtons(tmplName string, ctx locguide.Context) *FormButtons {
	fb := FormButtons{}
	fb.Buttons = []byte("")
	fb.tmplName = tmplName
	fb.ReturnPath = ctx.Request().Referer()
	return &fb
}

func (b *FormButtons) Render(data map[string]interface{}) error {
	t := template.Must(template.ParseFiles(b.tmplName))
	data["ReturnPath"] = b.ReturnPath
	err := t.Execute(b, data)
	return err
}

func (b *FormButtons) Write(buf []byte) (int, error) {
	b.Buttons = append(b.Buttons, buf...)
	return 0, nil
}
