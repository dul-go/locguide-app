package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type TypeaheadResult struct {
	ID        int
	Name      string
	Code      string
	Label     string
	RefObject string
	Slug      string
	RefType   string
}

func (t TypeaheadResult) String() string {
	return fmt.Sprintf("ID: %d | Name: %s | Code: %s | Label: %s | RefObject: %s | Slug: %s | RefType: %s\n",
		t.ID, t.Name, t.Code, t.Label, t.RefObject, t.Slug, t.RefType,
	)
}

var typeAheadSQL = "SELECT z.id, z.name, z.code, z.label, z.ref_object, z.slug, z.ref_type FROM " +
	"(" +
	"SELECT cg.id, cg.description as 'name', '' as code, cg.description as label, 'Collection Group' as ref_object, 'collectiongroups' AS slug, 'Collection Group' as ref_type " +
	"FROM locationguide_collectiongroup cg " +
	"WHERE INSTR(LOWER(cg.description), ?) > 0 " +
	"UNION " +
	"SELECT c.id, c.label as 'name', c.code, c.label, 'Collection' as ref_object, 'collections' AS slug, 'Collection' as ref_type " +
	"FROM locationguide_collection c " +
	"WHERE INSTR(LOWER(c.code), ?) > 0 " +
	"   OR INSTR(LOWER(c.label), ?) > 0 " +
	"UNION " +
	"SELECT s.id, s.label as 'name', s.code, s.label, 'Sublibrary' as ref_object, 'sublibraries' AS slug, 'Sublibrary' as ref_type " +
	"FROM locationguide_sublibrary s " +
	"WHERE INSTR(LOWER(s.code), ?) > 0 " +
	"   OR INSTR(LOWER(s.label), ?) > 0 " +
	"UNION " +
	"SELECT lm.id, lm.callno_range as 'name', NULL as code, CONCAT_WS(' ', lm.callno_range, " +
	"CONCAT_WS('', '(', lm.callno_start,'-',lm.callno_end,')')) as label, 'Call No. Location Map' as ref_object, 'locationmaps' as slug, " +
	"'Call No. Location Map' as ref_type " +
	"FROM locationguide_locationmap lm " +
	"WHERE INSTR(LOWER(lm.callno_range), ?) > 0 " +
	"   OR INSTR(LOWER(lm.callno_start), ?) > 0 " +
	"UNION " +
	"SELECT lm.id, cg.description as 'name', NULL as code, cg.description as label, " +
	"'Collection Location Map' as ref_object, 'locationmaps' as slug, " +
	"'Collection Location Map' as ref_type " +
	"FROM locationguide_locationmap lm " +
	"LEFT JOIN locationguide_collectiongroup cg ON cg.id = lm.collection_group_id " +
	"WHERE INSTR(LOWER(cg.description), ?) > 0 " +
	") z " +
	"ORDER BY z.label "

var callNumberTypeAheadSQL = "SELECT lm.id, lm.callno_range as 'name', '' as code, CONCAT_WS(' ', lm.callno_range, " +
	"CONCAT_WS('', '(', lm.callno_start,'-',lm.callno_end,')')) as label, 'Location Map' as ref_object, 'locationmap' as slug, " +
	"'Location Map' as ref_type " +
	"FROM locationguide_locationmap lm " +
	"WHERE (INSTR(LOWER(lm.callno_range), ?) > 0) " +
	"   OR (INSTR(LOWER(lm.callno_start), ?) > 0) " +
	"   OR (LOWER(lm.callno_start) <= ? AND LOWER(lm.callno_end) >= ?) " +
	"ORDER BY label"

// TypeaheadHandler will return a JSON-formatted list of
// entities based on the first letters typed in a UI field.
func (a *App) TypeaheadHandler(sid string) http.HandlerFunc {
	//conf := a.config
	return func(w http.ResponseWriter, r *http.Request) {
		c := a.NewContext(w, r)
		q := c.Param("q")

		svc := a.DBSvc
		result := []TypeaheadResult{}

		if sid == "searchbar" {
			svc.DB().Raw(typeAheadSQL, q, q, q, q, q, q, q, q).Scan(&result)
		} else {
			svc.DB().Raw(callNumberTypeAheadSQL, q, q, q, q).Scan(&result)
		}

		w.Header().Set("Access-Control-Allow-Origin", "*")
		//w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(result)
	}
}
