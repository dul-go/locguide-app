# Location Guide (Maps)

## Highlights
* The program functions as a full-service client/(web-)server application
* MySQL/MariaDB connections
* SAML/SSO integration embedded in the program
* More to come...

## Quickstart

### **Install `golang`.**  
Follow [these instructions](https://golang.org/doc/install)  

### Downloading Packages from Gitlab

**This step is very important!!!** 👇  
    
This project uses packages hosted on OIT's GitLab server. The steps below will ensure 
you're able to download these dependencies successfully.

#### **Create GitLab Personal Access Token**
Create a Personal Access Token with the following permissions:
- read_repository
- read_api
  
*Hang on to this token...*  

#### **Create `.netrc` File**
Create a `.netrc` file at the root of your home directory (or somewhere on 
your target VM when running in `production` mode). 
  
Then, add the following:
```sh
machine gitlab@oit.duke.edu
login git@gitlab.oit.duke.edu
password <your-newly-created-token>
```
  
**Now, you're ready to play...**

## Run Locally

### Non-Docker
```sh
$ git clone git@gitlab.oit.duke.edu:dul-go/locguide-app.git

$ cd /path/to/locguide-app

# copy the sample config, then edit as needed
# (e.g. change your db credentials)
$ cp config-sample.yml config.yml 

# update dependencies
$ go mod tidy

# see if she'll run (assumes non-SSL mode)
$ go run .

# To run in SSL mode on port 443
# 1) adjust the settings in your config.yml file, then
$ sudo go run .

# at some point when you need to run database migrations...
$ go run . -migrations
```

### Docker
```sh
$ [sudo] docker-compose up --build [--remove-orphans]
```

The project ships with a `docker-compose.yml` file, and a `sql` directory with data to test/develop with.

## Environmental Concerns
```
## Environment
For development, you can copy `config-sample.yml` to `config.yml`, edit the values to taste and run the application (`go run .`).

In production, however, it's best to set some environment variables (as well as using `config.yml`):
* `LOCGUIDE_BIND_HOST` - this is the binding host address when running the app (defaults to 0.0.0.0)
* `LOCGUIDE_BIND_PORT` - this is the port (say, 8443)
* `LOCGUIDE_DBUSER` - database user name
* `LOCGUIDE_DBPASS` - database password
* `LOCGUIDE_DBNAME` - database name

