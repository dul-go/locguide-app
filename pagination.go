package main

import (
	"fmt"
	"text/template"

	"gitlab.oit.duke.edu/dul-go/locguide-app/config"
)

// Pagination combines all the data needed to
// to calculate a pagination widget based on a model's count,
// plus the ability to render HTML
type Pagination struct {
	value interface{}

	Limit            int
	Page             int
	PreviousPage     int
	NextPage         int
	Sort             string
	TotalRows        int64
	TotalPages       int
	PagesPerChapter  int
	CurrentChapter   int
	PrevChapterEnd   int
	NextChapterBegin int
	Begin            int
	End              int
	PageRange        []int

	Content []byte
}

func NewPagination(e interface{}, conf config.Config) *Pagination {
	return &Pagination{
		value:           e,
		Content:         []byte(""),
		PagesPerChapter: 5,
		Sort:            "ID desc",
	}
}

func (p Pagination) String() string {
	return fmt.Sprintf("limit: %d\npage: %d\nsort: [%s]\ntotalrows: %d\ntotalpages: %d\npagesPerChapter: %d\ncurrentChapter: %d\nchapterBegin: %d\nchapterEnd: %d\nPrevChapterEnd: %d\nNextChapterBegin: %d\n",
		p.Limit, p.Page, p.Sort, p.TotalRows, p.TotalPages, p.PagesPerChapter, p.CurrentChapter, p.Begin, p.End, p.PrevChapterEnd, p.NextChapterBegin,
	)
}

func (p *Pagination) Write(buf []byte) (int, error) {
	p.Content = append(p.Content, buf...)
	return 0, nil
}

func (p *Pagination) Render(data map[string]interface{}) error {
	const pagerhtml = `
{{$Pager := .Pager}}
{{$WorkingURL := .WorkingURL}}
<div class="text-center">
<nav aria-label="Page navigation">
<ul class="pagination">
<li class="{{if eq .Pager.Page 1}}disabled{{end}}"><a aria-label="Previous" href="{{.WorkingURL}}?p={{.Pager.PreviousPage}}"><span aria-hidden="true">&laquo;</span></a></li>
<li class="{{if eq .Pager.Page 1}}disabled{{end}}"><a aria-label="First" href="{{.WorkingURL}}?p=1"><span aria-hidden="true">First</span></a></li>
{{if ne .Pager.Begin 1}}
<li><a aria-label="First Page" href="{{.WorkingURL}}?p=1"><span aria-hidden="true">1</span></a></li>
<li><a aria-label="Previous Chapter" href="{{.WorkingURL}}?p={{.Pager.PrevChapterEnd}}"><span aria-hidden="true">&hellip;</span></a></li>
{{end}}
{{range $k, $i := .Pager.PageRange}}
{{if eq $i $Pager.Page}}
<li class="active"><span>{{$i}} <span class="sr-only">(current)</span></span></li>
{{else}}
<li><a href="{{$WorkingURL}}?p={{$i}}">{{$i}} <span class="sr-only">Page {{$i}}</span></a></li>
{{end}}
{{end}}
{{if gt .Pager.TotalPages .Pager.End}}
<li><a aria-label="Next Chapter" href="{{.WorkingURL}}?p={{.Pager.NextChapterBegin}}"><span aria-hidden="true">&hellip;</span></a></li>
<li><a aria-label="Last Page" href="{{.WorkingURL}}?p={{.Pager.TotalPages}}"><span aria-hidden="true">{{.Pager.TotalPages}}</span></a></li>
{{end}}
<li class="{{if eq .Pager.TotalPages .Pager.Page}}disabled{{end}}"><a aria-label="Last" href="{{.WorkingURL}}?p={{.Pager.TotalPages}}"><span aria-hidden="true">Last</span></a></li>
<li class="{{if eq .Pager.NextPage .Pager.Page}}disabled{{end}}"><a aria-label="Next" href="{{.WorkingURL}}?p={{.Pager.NextPage}}"><span aria-hidden="true">&raquo;</span></a></li>
</ul>
</nav>
</div>
`
	t := template.Must(template.New("pagerhtml").Parse(pagerhtml))
	err := t.Execute(p, data)
	if err != nil {
		fmt.Println("Pagination::Render : error executing template: ", err)
	}
	return nil
}
