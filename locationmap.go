package main

import (
	"fmt"
	"text/template"

	"gitlab.oit.duke.edu/dul-go/httpresponse"
	"gitlab.oit.duke.edu/dul-go/journal"
	"gitlab.oit.duke.edu/dul-go/locguide-app/locguide"
	"gitlab.oit.duke.edu/dul-go/locguide-app/model"
)

var _ Controller = &LocationMapController{}

type LocationMapController struct{}

func (l *LocationMapController) PathPrefix() string {
	return "/locationmaps/"
}

func (l *LocationMapController) Listing(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	var locationMaps []model.LocationMap
	err := svc.FindThese(&locationMaps)

	if err != nil {
		return nil, fmt.Errorf("(LocationMap) Listing: unable to find location maps")
	}

	pageDocument := &httpresponse.PageDocument{
		Title: "Call Number & Collection Locations",
		Body:  []byte(""),
	}
	bodyTemplate := template.Must(template.ParseFiles("tmpl/locationmaps.go.html"))

	tmplData := map[string]interface{}{
		"LocationMaps":     locationMaps,
		"PlaceholderImage": "https://via.placeholder.com/150",
	}
	err = bodyTemplate.Execute(pageDocument, tmplData)
	if err != nil {
		return nil, fmt.Errorf("(LocationMap) Listing: unable to execute template - %v", err)
	}

	return pageDocument, nil
}

func (l *LocationMapController) Form(c locguide.Context) (*httpresponse.PageDocument, error) {
	app := c.Value("app").(*App)
	svc := app.DBSvc

	var locationMap model.LocationMap
	err := svc.FirstOrInit(&locationMap, c.IdForRequestedResource())
	journal.Debug.Printf("%s\n\n", locationMap)
	if err != nil {
		return nil, fmt.Errorf("(Location Map) Form: unable to load Location Map.")
	}

	areaMaps, _ := svc.AreaMaps()
	collectionGroups, _ := svc.CollectionGroups()
	externalLinks, _ := svc.ExternalLinks()

	c.AddTemplateData("LocationMap", locationMap)
	c.AddTemplateData("Entity", locationMap)
	c.AddTemplateData("SplitLMap", false)
	c.AddTemplateData("AreaMaps", areaMaps)
	c.AddTemplateData("CollectionGroups", collectionGroups)
	c.AddTemplateData("ExternalLinks", externalLinks)
	c.AddTemplateData("Priority", NewSlice(-10, 10, 1))
	c.AddTemplateData("SecondaryForm", "")

	if c.IsGetRequest() {
		// "success_message" and/or "error_message" could be
		// set when this handler is called from a POST request.
		//
		// In that case, those keys should still be in the session,
		// so let's grab 'em here.
		// TODO fix so we're not copying data from one part of "context"
		// to the other
		c.AddTemplateData("success_message", c.SessStringForKey("success_message"))
		c.AddTemplateData("error_message", c.SessStringForKey("error_message"))

		// determine if we have a "split (or copy)" flag present
		if c.Param("start-callno-split") == "1" {
			c.AddTemplateData("SplitLMap", &model.LocationMap{
				CollectionGroup:    locationMap.CollectionGroup,
				CallNumberRange:    locationMap.CallNumberRange,
				CallNumberEnd:      locationMap.CallNumberEnd,
				CallNumberType:     locationMap.CallNumberType,
				CallNumberPriority: locationMap.CallNumberPriority,
				AreaMap:            locationMap.AreaMap,
				ExternalLink:       locationMap.ExternalLink,
			})
			c.AddTemplateData("DisableSplitBtn", true)

			secondaryTemplate := template.Must(template.ParseFiles("tmpl/locationmap-secondary.go.html"))
			secondaryBlock := NewTemplateBlock("")
			secondaryTemplate.Execute(secondaryBlock, c.TemplateData())
			c.AddTemplateData("SecondaryForm", secondaryBlock.Content)
		} else {
			c.AddTemplateData("DisableSplitBtn", false)
		}
	}

	if c.IsPostRequest() {
		locationMap.ScanForm(c.Params(), "")
		if err = svc.Save(&locationMap); err != nil {
			return nil, fmt.Errorf("[LocationMap] Form: Unable to save Location Map record - %v", err)
		}

		var splitLocationMap model.LocationMap
		if c.Param("split_range") == "1" {
			svc.FirstOrInit(&splitLocationMap, "split")
			splitLocationMap.ScanForm(c.Params(), "split_")
			if err = svc.Save(&splitLocationMap); err != nil {
				return nil, fmt.Errorf("[LocationMap] Form: Unable to save split call number range record - %v", err)
			}
			c.SetSessionKey("success_message", "Changes have been saved. You are viewing the newly split record.")
			c.Redirect(fmt.Sprintf("/locationmaps/%d", splitLocationMap.ID), 303)
		} else {
			c.SetSessionKey("success_message", "Changes have been saved.")
			c.Redirect(fmt.Sprintf("/locationmaps/%d", locationMap.ID), 303)
		}
	}

	formButtons := NewFormButtons("tmpl/form_buttons.go.html", c)
	if err = formButtons.Render(c.TemplateData()); err != nil {
		panic(err)
	}
	c.AddTemplateData("FormButtons", formButtons.Buttons)
	c.AddTemplateData("CurrentPath", c.Data()["current_path"])

	tname := "tmpl/locationmap-primary.go.html"
	primaryTemplate := template.Must(template.ParseFiles(tname))
	primaryBlock := NewTemplateBlock("")
	if err = primaryTemplate.Execute(primaryBlock, c.TemplateData()); err != nil {
		panic(err)
	}
	c.AddTemplateData("PrimaryForm", primaryBlock.Content)
	//templateData["PrimaryForm"] = primaryBlock.Content

	pageDocument := &httpresponse.PageDocument{Title: "Location Map", Body: []byte("")}
	bodyTemplate := template.Must(template.ParseFiles("tmpl/locationmap.go.html"))
	err = bodyTemplate.Execute(pageDocument, c.TemplateData())
	if err != nil {
		return nil, fmt.Errorf("Form (LocationMap): unable to execute template - %v", err)
	}
	return pageDocument, nil
}

func (l *LocationMapController) DeleteHandler(c locguide.Context) (*httpresponse.PageDocument, error) {
	pageDocument := &httpresponse.PageDocument{Title: "Delete Location Map", Body: []byte("")}
	return pageDocument, nil
}
