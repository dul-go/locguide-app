package main

import "gitlab.oit.duke.edu/dul-go/httpresponse"

type Document struct {
	// Inherit from the legacy httpresponse.PageDocument
	httpresponse.PageDocument
}
