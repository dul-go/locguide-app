package main

type TemplateBlock struct {
	// Title display is left to the discretion
	// of the consuming party
	Title string

	Content []byte
}

func NewTemplateBlock(title string) *TemplateBlock {
	tb := &TemplateBlock{Title: title, Content: []byte("")}
	return tb
}

func (t *TemplateBlock) Write(buf []byte) (int, error) {
	t.Content = append(t.Content, buf...)
	return 0, nil
}
