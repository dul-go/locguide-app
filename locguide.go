package main

import (
	"crypto/rand"
	"encoding/base64"
	"flag"
	"fmt"
	_ "log"
	"os"
	"time"

	"gitlab.oit.duke.edu/dul-go/journal"
	"gitlab.oit.duke.edu/dul-go/locguide-app/config"
)

var appconfig config.Config

func init() {
}

func main() {

	// Variables to store command-line variables
	var (
		// FLAG: display config values, then exit app
		configOnly   bool
		confFilename string

		// Run data migrations and exit
		runMigrations bool

		// Generate session key and exit
		generateSessKey bool

		// a is the main application
		// it contains a 'Run' so as to run the web service
		a App
	)

	// Initialize logging for DEBUG, INFO, WARNING, ERROR
	// Note: for any one of these, you can use ioutil.Discard instead
	journal.InitLogging(os.Stdout, os.Stdout, os.Stdout, os.Stderr)

	// Parse flags as needed
	// Take note of how public member attributes of "a" (our application) are
	// also included in the parsing
	flag.BoolVar(&runMigrations, "migrations", false, "run data migrations and exit")
	flag.BoolVar(&generateSessKey, "gensesskey", false, "generate session key")
	flag.BoolVar(&a.Debug, "debug", false, "display debug verbosity")
	flag.BoolVar(&configOnly, "configonly", false, "display config settings and exit")
	flag.StringVar(&confFilename, "cfg", "config.yml", "location of config file...")
	flag.StringVar(&a.MainTemplateName, "tmpl", "tmpl/layouts/main.go.html", "location/path of main template file.")
	flag.StringVar(&a.AssetsDir, "assets-dir", "assets", "the directory to serve asset files from .")
	flag.DurationVar(&a.GracefulWait, "graceful-timeout", time.Second*15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")
	flag.Parse()

	// Read the config file
	var configData config.Config
	config.ReadConfigFile(&configData, confFilename)
	config.ReadEnv(&configData)
	journal.Debug.Println(configData)

	if generateSessKey {
		fmt.Println("Generating session key. Please store in an ENV variable on your system.")
		b := make([]byte, 16)
		_, err := rand.Read(b)
		if err != nil {
			os.Exit(1)
		}
		str := base64.URLEncoding.EncodeToString(b)
		fmt.Println(str)
		os.Exit(0)
	}

	// initialize the application, even in the event
	// we need to run migrations
	a.Initialize(configData)

	if runMigrations || configData.SetupDB {
		journal.Info.Println("Running database migrations for you....")
		db, _ := NewDatabaseService(configData)
		db.RunAutoMigrations()
		journal.Info.Println("Migrations done.")

		if runMigrations {
			os.Exit(0)
		}
	}

	// Run the app as a webserver
	a.Run()

	os.Exit(0)
}
